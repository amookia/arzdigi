package mongodb

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/amookia/arzdigi/BNBCHECKER/configs"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var MongoDB *mongo.Database
var CTX = context.TODO()

func init() {
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:27017", configs.MONGOURI))
	client, err := mongo.Connect(CTX, clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Ping(CTX, nil)
	if err != nil {
		log.Fatal(err)
	}
	MongoDB = client.Database("test")
}
