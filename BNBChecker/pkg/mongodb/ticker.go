package mongodb

import (
	"context"

	"gitlab.com/amookia/arzdigi/BNBCHECKER/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func InsertTicker(ticker models.Ticker) {
	filter := bson.M{"S": ticker.Ss}
	update := bson.M{"$set": ticker}
	option := options.Update().SetUpsert(true)
	MongoDB.Collection("ticker").UpdateOne(context.Background(), filter, update, option)
}
