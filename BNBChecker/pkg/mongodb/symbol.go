package mongodb

import (
	"gitlab.com/amookia/arzdigi/BNBCHECKER/internal/models"
	"go.mongodb.org/mongo-driver/bson"
)

func GetSymbols() []models.Symbol {
	var symbol []models.Symbol
	curosr, err := MongoDB.Collection("symbols").Find(CTX, bson.D{})
	if err != nil {
		panic(err)
	}
	err = curosr.All(CTX, &symbol)
	if err != nil {
		panic(err)
	}
	return symbol
}
