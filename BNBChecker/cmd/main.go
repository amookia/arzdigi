package main

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/amookia/arzdigi/BNBCHECKER/internal/binance"
	"gitlab.com/amookia/arzdigi/BNBCHECKER/pkg/mongodb"
)

func main() {
	syms := mongodb.GetSymbols()
	var symbols string
	for _, sym := range syms {
		symbols += fmt.Sprintf("/%s@ticker", strings.ToLower(sym.Abbreviation))
		log.Println(sym.Abbreviation)
	}
	binance.SymbolCheck(strings.ToLower(symbols))
}
