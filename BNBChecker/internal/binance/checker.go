package binance

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/amookia/arzdigi/BNBCHECKER/internal/models"
	"gitlab.com/amookia/arzdigi/BNBCHECKER/pkg/mongodb"
)

func SymbolCheck(path string) {
	u := url.URL{
		Scheme: "wss",
		Host:   "stream.binance.com:9443",
		Path:   "ws" + path,
	}
	fmt.Println(u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal(err)
	}
	if err != nil {
		log.Fatal(err)
	}
	for {
		_, message, err := c.ReadMessage()
		if err != nil {
			log.Fatal(err)
		}
		var ticker models.Ticker
		err = json.Unmarshal(message, &ticker)
		if err != nil {
			log.Fatal(err)
		}
		mongodb.InsertTicker(ticker)
		time.Sleep(1 * time.Second)
	}
}
