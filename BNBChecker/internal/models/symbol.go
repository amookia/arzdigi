package models

type Symbol struct {
	Abbreviation string `bson:"symbol"`
}
