package models

type Ticker struct {
	// S means small
	Se string `json:"e" bson:"e"`
	E  int64  `json:"E" bson:"E"`
	Ss string `json:"s" bson:"s"`
	Sp string `json:"p" bson:"p"`
	P  string `json:"P" bson:"P"`
	Sw string `json:"w" bson:"w"`
	X  string `json:"X" bson:"X"`
	Sc string `json:"c" bson:"c"`
	Q  string `json:"Q" bson:"Q"`
	Sb string `json:"b" bson:"b"`
	B  string `json:"B" bson:"B"`
	Sa string `json:"a" bson:"a"`
	A  string `json:"A" bson:"A"`
	So string `json:"o" bson:"o"`
	Sh string `json:"h" bson:"h"`
	Sl string `json:"l" bson:"l"`
	Sv string `json:"v" bson:"v"`
	Sq string `json:"q" bson:"q"`
	O  int64  `json:"O" bson:"O"`
	C  int64  `json:"C" bson:"C"`
	F  int64  `json:"F" bson:"F"`
	L  int64  `json:"L" bson:"L"`
	Sn int64  `json:"n" bson:"n"`
}
