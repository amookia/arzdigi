from src.models.mongodb import db
# {'symbol':'','tags':[''],'persian':'','english':''}
symbols = [
    {'symbol': 'SYSUSDT', 'tags': [
        'سیس کوین', 'syscoin'], 'persian':'سیس کوین', 'english':'syscoin'},

    {'symbol': 'UFTUSDT', 'tags': [
        'یونیلند', 'unilend', 'یونی لند'], 'persian':'یونی لند', 'english':'unilend'},

    {'symbol': 'BNUSDTT', 'tags': [
        'bnb', 'بی ان بی', 'بایننس'], 'persian':'بایننس', 'english':'bnb'},

    {'symbol': 'DOCKUSDT', 'tags': [
        'داک', 'dock'], 'persian':'داک', 'english':'dock'},

    {'symbol': 'BTCUSDT', 'tags': [
        'بیتکوین', 'bitcoin'], 'persian':'بیتکوین', 'english':'bitcoin'},

    {'symbol': 'DGUSDTT', 'tags': [
        'digibyte', 'دیجی بایت'], 'persian':'دیجی بایت', 'english':'digibyte'},

    {'symbol': 'XRPUSDT', 'tags': ['ریپل', 'ripple'],
        'persian':'ریپل', 'english':'ripple'},

    {'symbol': 'DOTUSDT', 'tags': [
        'polkadot', 'پولکا دات'], 'persian':'پولکا دات', 'english':'polkadot'},

    {'symbol': 'ETHUSDT', 'tags': [
        'ethereum', 'اتریوم'], 'persian':'اتریوم', 'english':'ethereum'},

    {'symbol': 'TRXUSDT', 'tags': ['ترون', 'tron'],
        'persian':'ترون', 'english':'tron'},

    {'symbol': 'DOGEUSDT', 'tags': [
        'دوج', 'doge coin', 'dogecoin', 'دوج کوین'], 'persian':'دوج', 'english':'dogecoin'},

    {'symbol': 'XMRUSDT', 'tags': ['مونرو', 'monero'],
        'persian':'مونرو', 'english':'monero'},

    {'symbol': 'SUSHIUSDT', 'tags': [
        'سوشی', 'sushi'], 'persian':'سوشی', 'english':'sushi'},

    {'symbol': 'UNIUSDT', 'tags': [
        'یونی سواپ', 'uniswap'], 'persian':'یونی سواپ', 'english':'uniswap'},

    {'symbol': 'DASHUSDT', 'tags': ['dash', 'دش'],
        'persian':'دش', 'english':'dash'},

    {'symbol': 'ETCUSDT', 'tags': ['اتریوم کلاسیک', 'ethereum classic'],
        'persian':'اتریوم کلاسیک', 'english':'ethereum classic'},

    {'symbol': 'LTCUSDT', 'tags': [
        'lightcoin', 'لایت کوین'], 'persian':'لایت کوین', 'english':'lightcoin'},

    {'symbol': 'BCHUSDT', 'tags': [
        'بیتکوین کش', 'bitcoin cash'], 'persian':'بیتکوین کش', 'english':'bitcoin cash'},

    {'symbol': 'ADAUSDT', 'tags': [
        'کاردانو', 'cardano', 'ada', 'ادا'], 'persian':'کاردانو', 'english':'cardano'},

    {'symbol': 'XLMUSDT', 'tags': [
        'استلار', 'stellar'], 'persian':'استلار', 'english':'stellar'},

    {'symbol': 'LINKUSDT', 'tags': [
        'chainlink', 'چین لینک'], 'persian':'چین لینک', 'english':'chainlink'},

    {'symbol': 'NEOUSDT', 'tags': ['neo', 'نئو'],
        'persian':'نئو', 'english':'neo'},

    {'symbol': 'IOTAUSDT', 'tags': [
        'آیوتا', 'ایوتا', 'iota'], 'persian':'ایوتا', 'english':'iota'},

    {'symbol': 'ZECUSDT', 'tags': ['زی کش', 'zcash'],
        'persian':'زی کش', 'english':'zcash'},

    {'symbol': 'XEMUSDT', 'tags': ['نم', 'nem'],
        'persian':'نم', 'english':'nem'},
        
    {'symbol': 'QTUMUSDT', 'tags': [
        'کوانتوم', 'qtum'], 'persian':'کوانتوم', 'english':'qtum'}
]


# prices -> rial
plans = [
    {'price': 400000, 'days': 30, 'description': '💎 اشتراک یک ماهه'},
    {'price': 800000, 'days': 60, 'description': '💎 اشتراک سه ماهه'},
    {'price': 120000, 'days': 120, 'description': '💎 اشتراک شش ماهه'}
]

admins = [
    {'name':'Pejman','chatid': 142792055 , 'privileges':
    {'receive_message':True,'price_update':True,'master':True}},
    {'name':'Kia','chatid': 98056633 , 'privileges':
    {'receive_message':True,'price_update':True,'master':True}},
    {'name':'Mostafa','chatid': 256185708 , 'privileges':
    {'receive_message':True,'price_update':True,'master':True}}

]


def start():
    # plans
    for plan in plans:
        try:
            db.plans.insert_one(plan)
        except:
            pass

    # symbols
    for symbol in symbols:
        try:
            db.symbols.insert_one(symbol)
        except:
            pass
    
    # admins
    for admin in admins:
        try:
            db.admins.insert_one(admin)
        except:
            pass