import config

indicators_keyboard = {"inline_keyboard":[
            [{"text":"RSI","callback_data":'indi:' + 'rsi'},{"text":"CCI","callback_data":'indi:' + 'cci'}],
            [{"text":"EMA","callback_data":'indi:' + 'ema'},{"text":'PSAR',"callback_data":'indi:' + 'psar'}],
            [{"text":"ICHIMOKU","callback_data": 'indi:' + 'ichimoku'},{"text":"MACD","callback_data":'indi:' + 'macd'}],
            [{"text":"BOLLINGER","callback_data": 'indi:' + 'bb'},{"text":"STOCHASTIC","callback_data":'indi:' + 'stoch'}]]}


basket_keyboard = {"inline_keyboard":[
    [{"text":"➕ افزودن ارز","callback_data":"addcr"},{"text":"❌ حذف ارز","callback_data":"removecr"}],
    [{"text":"⏰ تایم فریم ها","callback_data":"bskttmf"}]
]}


channel_keyboard = {"inline_keyboard":[
    [{"text":"join channel !","url":config.CHANNEL_URL}],
    [{"text":"✅ عضو شدم","callback_data":'joined'}]
]}


portfo_keyboard  = {"inline_keyboard":[
    [{"text":"🕵🏻‍♂️ بررسی کلی","callback_data":"prtanl"},{"text":"👨🏻‍💻 ارزهای من","callback_data":"prtme"}],
    [{"text":"➕ افزودن ارز","callback_data":"prtadd"}]
]}


admin_inline_keyboard = {"inline_keyboard":[
    [{"text":"⚡️اشتراک","callback_data":"adesh"},{"text":"📊 آمار کاربران","callback_data":"adstats"}],
    [{"text":"📺 کانال ها","callback_data":"adcnls"},{"text":"👨‍💻 ادمین ها","callback_data":"admins"}],
    [{"text":"🤹‍♀️ اضافه کردن ادمین","callback_data":"adcre"}],
    [{"text":"👱‍♂️ تنظیمات کاربری","callback_data":"aduser"}]
]}


subs_keyboard = {"inline_keyboard":[
    [{"text":"🕺 اعمال تخفیف","callback_data":"addiscount"},{"text":"👀 مشاهده پلن ها","callback_data":"adpwtch"}],
    [{"text":"➕ افزودن پلن جدید","callback_data":"adcrplan"},{"text":"🆓 اشتراک رایگان","callback_data":"adfree"}]
]}
