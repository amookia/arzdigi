from src.models.mongodb import db


#check if user has permisson to access admin functions
def admin_permission(original_function):
    def wrapper_function(data):
        if db.IsAdmin(data.chatid):
            return original_function(data)
    return wrapper_function