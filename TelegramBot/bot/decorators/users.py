from src.requests.vip import IsUserVip,IsUserPermitted
from src.models.mongodb import db

#check if user is vip or admin
def vip_permission(original_function):
    def wrapper_function(data,**kwargs):
        #if IsUserPermitted(data.chatid):
        #    return original_function(data,**kwargs)
        if IsUserVip(data.chatid) or db.IsAdmin(data.chatid) :
            return original_function(data,**kwargs)
    return wrapper_function