from flask import (Blueprint, jsonify, request)
from .handlers import (message,callback,channel)

telegram = Blueprint('telegram', __name__, url_prefix='/bot/')


@telegram.route('/listen', methods=['POST'])
def listen():
    json_data = request.get_json()

    if 'message' in json_data:
        message_json = json_data['message']
        if 'text' in message_json : message.message_handler(json_data)

    if 'callback_query' in json_data:
        callback.callback_handler(json_data)
    
    if 'channel_post' in json_data:
        channel.channel_handler(json_data)

    return jsonify({'message': 'OK'})
