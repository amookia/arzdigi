from .serializer import message_serializer
from .status import stats_handler
from ..telegram import messages
from src.heatmap.map import send_map
from src.requests.channel import IsJoined
from src.models.mongodb import db
from threading import Thread
import config

class message_handler:
    def __init__(self, json_data):
        data = message_serializer(json_data)
        if data.reply_to_chatid is not None and \
             data.chatid in config.ADMINS: messages.admin_answer(json_data)

        elif IsJoined(data.chatid) != True:
            messages.join_channel(data)

        elif data.text in ('/start','↩️ بازگشت'):
            messages.user_start(data)

        elif data.text == '📈 قیمت لحظه‌ای':
            messages.user_price_check(data)
        
        elif data.text == '🗺 نقشه بازار':
            Thread(send_map(data.chatid)).start()

        elif data.text == 'ارتباط با ادمین 🗣':
            messages.user_contact_admin(data)
        
        elif data.text == '🎫 اشتراک':
            messages.user_subs(data)
        
        elif data.text == '🧮 سیگنال اندیکاتور':
            messages.signal_andicator(data)
        
        elif data.text == '🥇 سیگنال استراتژی':
            messages.signal_strategy(data)
        
        elif data.text == '👀 واچ‌لیست':
            messages.user_basket(data)
        
        elif data.text == '🧺 پورتفو':
            messages.user_portfo(data)
        
        elif data.text == '💸 سیگنال وی آی پی':
            messages.user_signal_vip(data)

        elif data.text == '👨‍🚀 ادمین':
            if db.IsAdmin(data.chatid):
                messages.admin_run(data)
        
        elif data.text[:13] == '/start touser':
            if db.IsAdmin(data.chatid):
                messages.admin_contact(data)
        
        elif data.text == '📖 راهنما':
            messages.help(data)

        else:
            stats_handler(data)

