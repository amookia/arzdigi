from .serializer import callback_serializer
from ..telegram import callbacks,messages
from src.models.mongodb import db
from ..decorators import admin,users

class callback_handler:
    def __init__(self, json_data):
        self.data = callback_serializer(json_data)

        if self.data.callback_data == 'joined' : messages.join_channel(self.data,True)
        elif self.data.callback_data[:3] in ('pch','rch'): callbacks.callback_check_price(self.data)
        else:
            self.admin(self.data)
            self.vip_user(self.data)

    @staticmethod
    @admin.admin_permission
    def admin(data):
        if data.callback_data == 'adstats' : callbacks.callback_admin_statistics(data)
        elif data.callback_data == 'admins' : callbacks.callback_admin_lists(data)
        elif data.callback_data[:4] == 'adch' : callbacks.callback_admin_setting(data)
        elif data.callback_data[:5] == 'adprv' : callbacks.callback_admin_prvs(data)
        elif data.callback_data[:5] == 'addel' : callbacks.callback_admin_del(data)
        elif data.callback_data[:5] == 'adprs' : callbacks.callback_admin_prvs_update(data)
        elif data.callback_data == 'adcre' : callbacks.callback_admin_create(data)
        elif data.callback_data[:8] == 'adcreate' : callbacks.callbacks_admin_insert(data)
        elif data.callback_data == 'adesh' : callbacks.callback_admin_subs(data)
        elif data.callback_data == 'adcrplan' : callbacks.callback_admin_subs_crplan(data)
        elif data.callback_data == 'adpwtch' : callbacks.callback_admin_subs_plans(data)
        elif data.callback_data[:5] == 'admch' : callbacks.callback_admin_subs_planshow(data)

        elif data.callback_data[:8] == 'adplndel' : callbacks.callback_admin_subs_plandel(data)
        elif data.callback_data[:7] == 'adplned' : callbacks.callback_admin_subs_planedit(data)
        elif data.callback_data == 'addiscount' : callbacks.callback_admin_subs_discount(data)
        elif data.callback_data == 'crdiscount' : callbacks.callback_admin_subs_discount_add(data)
        elif data.callback_data == 'rmvdiscount' : callbacks.callback_admin_subs_discount_rmv(data)
        elif data.callback_data == 'adfree' : callbacks.callback_admin_subs_free(data)
        elif data.callback_data == 'adcnls' : callbacks.callback_admin_channels(data)
        elif data.callback_data == 'adcrch' : callbacks.callback_admin_channels_create(data)
        elif data.callback_data[:6] == 'adrmvh' : callbacks.callback_admin_channels_rmv(data)
        elif data.callback_data == 'aduser' : callbacks.callback_admin_users_check(data)
        elif data.callback_data[:7] == 'prtovip' : callbacks.callback_admin_tovip(data)
        elif data.callback_data[:9] == 'removevip' : callbacks.callback_admin_removevip(data)
        elif data.callback_data[:9] == 'checkuser' : callbacks.callback_admin_userinfo(data)

    @staticmethod
    @users.vip_permission
    def vip_user(data):
        if data.callback_data in ('returninc','indicators') : messages.signal_andicator(data,callback=True)
        elif data.callback_data[:4] == 'indc' : callbacks.callback_indicators_update(data)
        elif data.callback_data == 'timeframes' : callbacks.callback_timeframes(data,'tmfr')
        elif data.callback_data[:4] == 'tmfr' : callbacks.callback_timeframes_update(data,'tmfr')
        elif data.callback_data == 'buynsell' : callbacks.callback_buynsell(data,'sbns')
        elif data.callback_data[:8] == 'checkbns' : callbacks.callback_bns_update(data,'sbns')

        elif data.callback_data == 'returnsigs' : messages.signal_strategy(data,callback=True)
        elif data.callback_data[:4] == 'sigs' : callbacks.callback_strategies_update(data)
        elif data.callback_data == 'sgstimeframes' : callbacks.callback_timeframes(data,'sifr')
        elif data.callback_data[:4] == 'sifr' : callbacks.callback_timeframes_update(data,'sifr')
        elif data.callback_data[:11] == 'sgsbuynsell' : callbacks.callback_buynsell(data,'sifr')
        elif data.callback_data[:9] == 'scheckbns' : callbacks.callback_bns_update(data,'sifr')
        
        #watchlist
        elif data.callback_data[:5] == 'addcr' : callbacks.callback_watchlist_add(data)
        elif data.callback_data[:8] == 'removecr' : callbacks.callback_watchlist_remove(data)
        elif data.callback_data[:7] == 'bskttmf' : callbacks.callback_watchlist_timeframe(data)
        elif data.callback_data[:7] == 'bsktime' : callbacks.callback_update_watchlist_time(data)
        elif data.callback_data[:7] == 'wtchadd' : callbacks.callback_watchlist_add_db(data)
        elif data.callback_data[:6] == 'rwatch' : callbacks.callback_watchlist_remove_db(data)

        #portfo
        elif data.callback_data == 'prtadd' : callbacks.callback_portfo_setstat(data)
        elif data.callback_data[:7] == 'prtfadd' : callbacks.callback_portfo_add(data)
        elif data.callback_data == 'prtme' : callbacks.callback_portfo_me(data)
        elif data.callback_data[:4] == 'prtf' : callbacks.callback_portfo_detail(data)
        elif data.callback_data == 'prtanl' : callbacks.callback_portfo_analyze(data)
        elif data.callback_data[:4] == 'prte' : callbacks.callback_portfo_exit(data)#

        #signal vip
        elif data.callback_data[:9] == 'signalvip' : callbacks.callback_user_signalvip(data)