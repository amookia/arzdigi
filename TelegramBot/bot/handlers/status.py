import config
from bson.objectid import ObjectId
from ..keyboards.keyboards import (normal_keyboard,return_keyboard,admin_keyboard)
from src.models.mongodb import db
from src.requests import telegram as sender
from datetime import datetime, timedelta
import pymongo
from payments.vipmembers import vip

class stats_handler:
    def __init__(self, data):
        self.data = data
        self.stat = db.members.find_one({'chatid': data.chatid})['stat']
        print('stats handler -> ', data.chatid, ' -> ', self.stat)
        print(len(self.stat))

        if self.stat == 'pricecheck' : self.pricecheck()
        elif self.stat == 'contact' : self.contact_admin()
        elif self.stat == 'watchlist' : self.watchlist_add()
        elif self.stat == 'portfoadd' : self.portfo_add()
        elif self.stat[:8] == 'portfoup' : self.portfo_up()
        elif self.stat[:4] == 'prte' : self.portfo_exit()
        elif self.stat == 'createadmin' : self.create_admin()
        elif self.stat[:10] == 'createplan' : self.create_plan()
        elif self.stat[:6] == 'edtpln' : self.update_plan()
        elif self.stat == 'crdiscount' : self.discount()
        elif self.stat[:8] == 'chcreate' : self.create_channel()
        elif self.stat == 'adminfreevip' : self.admin_free_vip()
        elif self.stat[:7] == 'adminct' : self.admin_call()
        elif self.stat == 'aduser' : self.admin_user()
        elif self.stat[:9] == 'updatevip' : self.admin_promote()
        #handle this line later (invalid messages)
        #else: messages.invalid_message(data)
 
    def pricecheck(self):
        try:
            text = self.data.text.upper()
            isalpha = self.data.text.isalpha() == False
            symbols = db.tickers.find({'s': {'$regex': text}})
            inline = list()
            for symbol in symbols:
                text = '🔅 ' + symbol['s']  + ' 🔅'
                inline.append([{'text': text, 'callback_data': 'pch:' + symbol['s']}])
                if len(inline) == 5:
                    break

            if len(inline) == 0 or isalpha:
                text = '📊 نماد مورد نظر شما پیدا نشد'
                sender.SendMessage(self.data.chatid, text)
            else:
                text = '👇 ارز مورد نظر خود را انتخاب کنید'
                inline = {'inline_keyboard': inline}
                sender.SendMessage(self.data.chatid, text, inline)
        except:
            pass
    

    def contact_admin(self):
        try:
            admins = db.admins.find({'privileges.receive_message':True})
            for admin in admins:
                admin_id = admin['chatid']
                from_chatid = self.data.chatid
                message_id = self.data.message_id
                url = f'https://t.me/{config.BOT_USERNAME}?start=touser_{str(from_chatid)}_{message_id}'
                text_msg = self.data.text + '\n' + f'[ارسال پاسخ]({url})'
                inline = {'inline_keyboard': [[{'text':'💁 اطلاعات کاربری','callback_data':f'checkuser:{str(from_chatid)}'}]]}
                sender.SendMessage(admin_id,text_msg,parse_mode='Markdown',keyboard=inline)
            text = '✅ پیام شما با موفقیت ارسال شد'
            sender.SendMessage(self.data.chatid,text,normal_keyboard)
        except Exception as e:
            print(e)
    

    def watchlist_add(self):
        text = self.data.text.upper()
        isalpha = self.data.text.isalpha() == False
        try:
            symbols = db.tickers.find({'s': {'$regex': text}})
            inline = list()
            for symbol in symbols:
                text = '🔅 ' + symbol['s']  + ' 🔅'
                inline.append([{'text': text, 'callback_data': 'wtchadd:' + symbol['s']}])
                if len(inline) == 5:
                    break

            if len(inline) == 0 or isalpha:
                text = 'ارز مورد نظر پیدا نشد'
                sender.SendMessage(self.data.chatid, text)
            else:
                text = '👇برای اضافه شدن ارز مورد نظر به سبد یکی از گزینه های زیر را انتخاب کنید'
                inline = {'inline_keyboard': inline}
                sender.SendMessage(self.data.chatid, text, inline)
        except:
            pass
        
    
    def portfo_add(self):
        try:
            text = self.data.text.upper()
            isalpha = self.data.text.isalpha() == False
            symbols = db.tickers.find({'s': {'$regex': text}})
            inline = list()
            for symbol in symbols:
                text = '🔅 ' + symbol['s'] + ' 🔅'
                inline.append([{'text': text, 'callback_data': 'prtfadd:' + symbol['s']}])
                if len(inline) == 5:
                    break

            if len(inline) == 0 or isalpha:
                text = 'ارز مورد نظر پیدا نشد'
                sender.SendMessage(self.data.chatid, text)
            else:
                text = '👇برای اضافه شدن ارز مورد نظر به سبد یکی از گزینه های زیر را انتخاب کنید'
                inline = {'inline_keyboard': inline}
                sender.SendMessage(self.data.chatid, text, inline)
        except:
            pass

    
    def portfo_up(self):
        splited = self.stat.split(':')
        if len(splited) > 2:
            symbol = splited[1]
            price = float(splited[2])
            try:
                volume = float(self.data.text)
                text = '✅ ارز با موفقیت به پورتفو اضافه شد'
                #check if exists
                user_portfo = db.portfo.find_one({'chatid':self.data.chatid,'symbol':symbol,'exit':False})
                if user_portfo is None:
                    db.portfo.insert_one({'chatid':self.data.chatid,'symbol':symbol,
                    'buy':price,'sell':None,'volume':volume,'exit':False})
                else:
                    user_volume = float((volume + user_portfo['volume']) / 2)
                    user_buy = float((price + user_portfo['buy']) / 2)
                    db.portfo.update_one({'chatid':self.data.chatid,'symbol':symbol},
                    {'$set':{'volume':user_volume,'buy':user_buy}})

                db.update_stat(self.data.chatid,'main')
                sender.SendMessage(self.data.chatid,text,normal_keyboard)
            except:
                text = '''❗️حجم وارد شده اشتباه میباشد
            به صورت زیر قیمت را وارد کنید
            1234'''
        else:
            symbol = self.stat[9:]
            try:
                price = float(self.data.text)
                text = '👇تعداد خرید خود را وارد کنید.'
                db.update_stat(self.data.chatid,'portfoup:'+ symbol + ':' + str(price))
            except Exception as e:
                print(e)
                text = '''❗️قیمت وارد شده اشتباه میباشد
    به صورت زیر قیمت را وارد کنید
    1234'''
            sender.SendMessage(self.data.chatid,text,return_keyboard)


    def portfo_exit(self):
        id = self.stat[5:]
        try:
            sell_price = float(self.data.text)
            db.portfo.update_one(
                {'_id':ObjectId(id),'chatid':self.data.chatid},
                {'$set':{'sell':sell_price,'exit':True}})
            text = '💰 ارز شما با موفقیت از پورتفو خارج شد.'
            db.update_stat(self.data.chatid,'main')
            keyboard = normal_keyboard
        except:
            text = 'فرمت وارد شده اشتباه میباشد ❗️'
            keyboard = return_keyboard
        
        sender.SendMessage(self.data.chatid,text,keyboard)
    

    def create_admin(self):
        if db.IsAdmin(self.data.chatid):
            try:
                target_chatid = int(self.data.text)
                member = db.members.find_one({'chatid':target_chatid})
                if member is not None and db.IsAdmin(target_chatid) != True:
                    first_name = member['first_name']
                    text = f'نام : {first_name}'
                    keyboard = {"inline_keyboard":[
                        [{"text":'✅ تایید',"callback_data":f'adcreate:{str(target_chatid)}'}]
                    ]}
                    sender.SendMessage(self.data.chatid,text,keyboard)
                #else:
                #    text = 'کاربر داخل ربات عضو نشده است یا ادمین میباشد'

                #db.update_stat(self.data.chatid,'main')
                #sender.SendMessage(self.data.chatid,'admin',admin_keyboard)

            except:
                text = 'فرمت نامعتبر'
                sender.SendMessage(self.data.chatid,text,return_keyboard)
    

    def create_plan(self):
        split_text = self.stat.split(':')
        try:
            #days
            if len(split_text) == 1 :
                days = int(self.data.text)
                db.update_stat(self.data.chatid,f'createplan:{str(days)}')
                text = 'قیمت را به ریال وارد کنید'
            #price
            elif len(split_text) == 2:
                days = split_text[1]
                price = int(self.data.text)
                db.update_stat(self.data.chatid,f'createplan:{str(days)}:{str(price)}')
                text = 'اطلاعات اشتراک را وارد کنید' + '\nمثلا : اشتراک یک ماهه'
            #description and insert_db
            elif len(split_text) == 3:
                days = int(split_text[1])
                price = int(split_text[2])
                description = str(self.data.text)
                db.plans.insert_one({'price':price,'days':days,'description':description})
                text = 'اشتراک با موفقیت ساخته شد'
                
        except pymongo.errors.DuplicateKeyError as e:
            text = 'روز اشتراک تکراری میباشد'
            
        except ValueError as verr:
            text = 'فرمت نامعتبر'
        
        sender.SendMessage(self.data.chatid,text,return_keyboard)
    

    def update_plan(self):
        split_text = self.stat.split(':')
        try:
            if len(split_text) == 2:
                days = int(self.data.text)
                db.update_stat(self.data.chatid,f'{self.stat}:{str(days)}')
                text = 'قیمت را به ریال وارد کنید'
            elif len(split_text) == 3:
                days = split_text[2]
                price = int(self.data.text)
                db.update_stat(self.data.chatid,f'{self.stat}:{str(price)}')
                text = 'اطلاعات اشتراک را وارد کنید' + '\nمثلا : اشتراک یک ماهه'
            elif len(split_text) == 4:
                id = str(split_text[1])
                days = int(split_text[2])
                price = int(split_text[3])
                description = str(self.data.text)
                text = 'اشتراک با موفقیت ویرایش شد'
                db.plans.update_one({'_id':ObjectId(id)},
                    {'$set':{'days':days,'price':price,'description':description}})
            
        except ValueError as verr:
            text = 'فرمت نامعتبر'
        
        sender.SendMessage(self.data.chatid,text,return_keyboard)
    

    def discount(self):
        try:
            plans = db.plans.find()
            percentage = int(self.data.text)
            datas = []
            for planx in plans:
                datas.append([planx])
            db.rules.insert_one({'rule':'discount','persent':percentage,'plans':datas})
            plans = db.plans.find()
            for plan in plans:
                price = plan['price']
                updated_price = int(price - (percentage * price) / 100)
                db.plans.update_one({'price':price},{'$set':{'price':updated_price}})
            text = 'تخفیفف اعمال شد'
        
        except pymongo.errors.DuplicateKeyError as e:
            text = 'متاسفانه تخفیف اعمال شده'

        except ValueError as verr:
            text = 'فرمت نامعتبر'
        
        sender.SendMessage(self.data.chatid,text,return_keyboard)


    def create_channel(self):
        split_text = self.stat.split('|')
        try:
            if len(split_text) == 1:
                channel_id = int(self.data.text)
                db.update_stat(self.data.chatid,f'{self.stat}|{str(channel_id)}')
                text = 'لینک کانال را وارد کنید'
            elif len(split_text) == 2:
                link = str(self.data.text)
                db.update_stat(self.data.chatid,f'{self.stat}|{link}')
                text = 'توضیحات کانال را وارد کنید'
            elif len(split_text) == 3:
                datas = self.stat.split('|')
                channel_id = datas[1]
                link = datas[2]
                details = self.data.text
                db.channels.insert_one({'channel_id':channel_id,'link':link,'details':details})
                text = 'کانال با موفقیت اضافه شد' + '\n' + 'لطفا ربات را در کانال ادمین کنید'
        except:
            text = 'فرمت نامعتبر'
        
        sender.SendMessage(self.data.chatid,text,return_keyboard)
    

    def admin_free_vip(self):
        try:
            hours = int(self.data.text)
            unixtime = datetime.now() + timedelta(hours=hours)
            unixtime = unixtime.timestamp()
            db.rules.find_one_and_update({'rule':'freevip'},
            {'$set':{'rule':'freevip','expire_at':unixtime}},upsert=True)
            text = '✅ تغییرات اعمال شد'
        except:
            text = 'فرمت نامعتبر'
        sender.SendMessage(self.data.chatid,text,return_keyboard)
    

    def admin_call(self):
        chatid = self.stat[8:].split(':')[0]
        text = 'پیام ادمین 👇' + '\n\n'
        text += self.data.text
        sender.SendMessage(chatid,text)
    

    def admin_user(self):
        text = self.data.text
        chatid = self.data.chatid
        try:
            int(text)
            print('int text',text)
            user = db.members.find_one({'chatid':int(text)})
            if user is not None:
                text_chat = 'انتخاب کنید'
                keyboard = {'inline_keyboard':[
                    [{'text':'💁 اطلاعات کاربری','callback_data':f'checkuser:{str(text)}'}],
                    [{'text':'🤑 اشتراک','callback_data':f'prtovip:{str(text)}'}]]}
                sender.SendMessage(chatid,text_chat,keyboard)
            else :
                 text_chat = 'کاربر یافت نشد'
                 sender.SendMessage(chatid,text_chat)
        except:
            text_chat = 'فرمت نامعتبر'
            sender.SendMessage(chatid,text_chat)
    

    def admin_promote(self):
        text = self.data.text
        chatid = self.stat.split(':')[1]
        try:
            vip.user_to_vip_day(int(chatid),int(text))
            text = f'{text} روز اضافه شد'
        except Exception as e:
            text = 'فرمت نامعتبر'
        sender.SendMessage(self.data.chatid,text)