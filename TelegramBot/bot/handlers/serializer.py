import persian

class message_serializer:
    def __init__(self,json_data):
        try:
            self.update_id = json_data['update_id']
            message = json_data['message']
            self.message_id = message['message_id']
            self.chatid = message['from']['id']
            self.first = ''
            self.username = None
            self.text = ''
            self.reply_to_chatid = None
            if 'username' in message['from'] : self.username = message['from']['username']
            if 'text' in message : self.text = str(message['text'])
            if 'reply_to_message' in message : self.reply_to_chatid = message['reply_to_message']['from']['id']
            if 'first_name' in message['from'] : self.first = message['from']['first_name']
            self.text = persian.convert_ar_characters(self.text)
            self.text = persian.convert_ar_numbers(self.text)
            self.text = persian.convert_fa_numbers(self.text)
        except:
            self.message_id = ''
            self.chatid = ''
            self.first = ''
            self.username = None
            self.text = ''
            self.reply_to_chatid = None

class callback_serializer:
    def __init__(self,json_data):
        self.callback_query = json_data['callback_query']
        self.update_id = json_data['update_id']
        self.callback_id = self.callback_query['id']
        self.chatid = self.callback_query['from']['id']
        self.message_id = self.callback_query['message']['message_id']
        self.callback_data = self.callback_query['data']
        self.first = ''
        if 'first_name' in self.callback_query['from'] : self.first = self.callback_query['from']['first_name']

class channel_serializer:
    def __init__(self,json_data):
        self.update_id = json_data['update_id']
        channel_post = json_data['channel_post']
        self.message_id = channel_post['message_id']
        sender_chat = channel_post['sender_chat']
        self.sender_id = sender_chat['id']
        try:
            self.text = channel_post['text']
        except:
            self.text = ''