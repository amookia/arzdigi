from .serializer import channel_serializer
from src.models.mongodb import db
from src.requests.telegram import CopyMessage
import time
from threading import Thread


class channel_handler:
    def __init__(self,json_data):
        pass
        #data = channel_serializer(json_data)
        # self.channel_id = -1001771816509

        # if str(data.sender_id) == '-1001771816509' : 
        #     self.message_id = data.message_id
        #     if '#vip' in data.text:
        #         self.send_to_vips()
        #     else:
        #         self.send_to_users()

    

    def send_to_vips(self):
        vips = db.vip.find({'expire':{'$gt':time.time()}})
        for vip in vips:
            chatid = vip['chatid']
            Thread(CopyMessage(chatid,self.channel_id,self.message_id)).start()
    
    
    def send_to_users(self):
        users = db.members.find()
        for user in users:
            chatid = user['chatid']
            Thread(CopyMessage(chatid,self.channel_id,self.message_id)).start()
