from config import WEB_URL
from src.requests import telegram as sender
from src.models.mongodb import db
from ..keyboards.keyboards import (normal_keyboard, return_keyboard , admin_keyboard)
from ..keyboards.inline_keyboards import (basket_keyboard,channel_keyboard,portfo_keyboard,admin_inline_keyboard)
from bson import ObjectId
from payments.vipmembers import vip
from ..texts import persian
from persiantools.jdatetime import JalaliDateTime
import datetime
import config
from ..decorators import admin,users


# when user /start or بازگشت
def user_start(data):
    is_admin = db.admins.find_one({'chatid':data.chatid}) is not None
    is_user = db.members.find_one({'chatid': data.chatid}) is None
    if is_admin:
        text = 'شما ادمین هستید !'
        keyboard = admin_keyboard
        db.update_stat(data.chatid, 'main')
    elif is_user:
        member = {'chatid': data.chatid,
                  'first_name': data.first, 'stat': 'main'}
        db.members.insert_one(member)
        text = 'به ربات هوشمند ارز دیجیتال IRcryptoo خوش آمدید' + '\n' + '⬇️ انتخاب کنید'
        keyboard = normal_keyboard
    else:
        text = '⬇️ انتخاب کنید'
        db.update_stat(data.chatid, 'main')
        keyboard = normal_keyboard

    sender.SendMessage(data.chatid, text, keyboard)


def invalid_message(data):
    text = 'دستور اشتباه❗️'
    sender.SendMessage(data.chatid,text,reply_to=data.message_id)


def help(data):
    file_id = 'BAACAgQAAxkBAAEJbGdhm3WrUFn6aCK0NohDyWPuDPDSIAACuQwAApR62VAVY6WbstGWgiIE'
    sender.SendVideo(data.chatid,file_id,persian.help_text)


#force join channel
def join_channel(data,joined=False):
    sender.RemoveMessage(data.chatid,data.message_id)
    keyboard = []
    channels = db.channels.find()
    checker = True
    for channel in channels:
        channel_id = channel['channel_id']
        desc = channel['details']
        link = channel['link']
        checker = checker & sender.MemberJoined(channel_id,data.chatid)
        keyboard.append([{'text':desc,'url':link}])
    keyboard.append([{"text":"✅ عضو شدم","callback_data":'joined'}])
    keyboard = {'inline_keyboard':keyboard}

    if checker:
        user_start(data)
    else:
        text = 'برای بهره‌مندی از خدمات ربات لطفا عضو کانار اختصاصی ما شوید👇'
        sender.SendMessage(data.chatid,text,keyboard)


# check for prices
def user_price_check(data):
    db.update_stat(data.chatid, 'pricecheck')
    text = '🥇نماد ارز مورد نظر خود را وارد کنید'+ '\n' + '💱برای مثال BTCUSDT' + '\n.'
    sender.SendMessage(data.chatid, text, return_keyboard)


def user_contact_admin(data):
    text = '🔰 لطفا پیام خود را ارسال کنید'
    db.update_stat(data.chatid, 'contact')
    sender.SendMessage(data.chatid, text, return_keyboard)


def user_subs(data):
    keyboard = []
    plans = db.plans.find()
    for plan in plans:
        user_id = str(db.members.find_one({'chatid':data.chatid})['_id'])
        price = str(int(plan['price']/10))
        inline_text = plan['description'] + f' {price} تومان'
        url = WEB_URL + '/pay/generate/' + str(plan['price']) + '/' + user_id
        keyboard.append([{"text": inline_text,'url': url}])
    keyboard = {'inline_keyboard':keyboard}
    user = vip.vip_member(data.chatid)
    if user.is_vip:
        unixdate = datetime.datetime.fromtimestamp(user.expire)
        date = JalaliDateTime.to_jalali(unixdate).strftime('%H:%M %Y/%m/%d')
        text = persian.vip_sub.format(date)
    else:
        text = persian.free_sub_user.format(data.first)
    sender.SendMessage(data.chatid,text,keyboard)


def admin_answer(data):
    message = data['message']
    from_chatid = message['from']['id']
    try:
        messageid = message['message_id']
        reply_to_message = message['reply_to_message']
        forward_sender_name = reply_to_message['forward_sender_name']
        date = reply_to_message['date']
        forward_date = reply_to_message['forward_date']
        forwarded_message = db.contacts.find_one(
            {'forward_sender_name': forward_sender_name, 'forward_date': forward_date})
        to_chatid = forwarded_message['from_chatid']
        to_messageid = forwarded_message['messageid']
        #db.contacts.delete_one({'_id': ObjectId(forwarded_message['_id'])})
        sender.CopyMessage(to_chatid, from_chatid, messageid, to_messageid)
    except Exception as e:
        text = '❗️متاسفانه پیام ارسال نشد'
        sender.SendMessage(from_chatid,text)



@users.vip_permission
def signal_strategy(data,callback=None):
    user = db.strategies.find_one({'chatid':data.chatid})
    text = 'استراتژی مورد نظر خود را انتخاب کنید' + '\n✅ فعال' + '\n❌ غیرفعال'
    if user is not None:
        bs = user['bs']
        br = user['br']
        rsm = user['rsm']
        ecc = user['ecc']
    else:
        bs = True
        br = True
        rsm = True
        ecc = True
        db.strategies.insert_one({'chatid':data.chatid,
        'bs':bs,'br':br,'rsm':rsm,'ecc':True,'resolutions':['15m','1h','4h','1d'],'buy':True,'sell':True})
    
    bs_text = '✅ BS' if bs else '❌ BS'
    br_text = '✅ BR' if br else '❌ BR'
    rsm_text = '✅ RSM' if rsm else '❌ RSM'
    ecc_text = '✅ EMA CCI' if ecc else '❌ EMA CCI'
    keyboard = {'inline_keyboard':[
        [{'text':bs_text,'callback_data':f'sigs:bs:{str(bs)}'},{'text':br_text,'callback_data':f'sigs:br:{str(br)}'}],
        [{'text':rsm_text,'callback_data':f'sigs:rsm:{rsm}'},{'text':ecc_text,'callback_data':f'sigs:ecc:{ecc}'}],
        [{'text':'⏱ تایم فریم ها','callback_data':'sgstimeframes'}],
        [{'text':'💸 خرید و فروش','callback_data':'sgsbuynsell'}]
    ]}

    if callback is None:
        sender.SendMessage(data.chatid,text,keyboard)
    else:
        sender.EditMessage(data.chatid,data.message_id,text,keyboard)


@users.vip_permission
def signal_andicator(data,callback=None):
    text = '❌اندیکاتور غیرفعال\n✅ اندریکاتور فعال' + '\n.'
    user = db.indicators.find_one({'chatid':data.chatid})
    if user is not None:
        rsi = user['rsi']
        cci = user['cci']
        ema = user['ema']
        ichimoku = user['ichimoku']
        macd = user['macd']
        psar = user['psar']
        bollinger = user['bollinger']    
        stochastic = user['stochastic'] 
    else:
        rsi = True
        cci = True
        ema = True
        ichimoku = True
        macd = True
        psar = True
        stochastic = True
        bollinger = True
        db.indicators.insert_one({'chatid':data.chatid,'resolutions':['1h'],
        'rsi':True,'cci':True,'ema':True,'ichimoku':True,'macd':True,
        'psar':True,'stochastic':True,'bollinger':True,'sell':True,'buy':True})
    
    rsi_text = '✅ RSI' if rsi else '❌ RSI'
    cci_text = '✅ CCI' if cci else '❌ CCI'
    ema_text = '✅ EMA' if ema else '❌ EMA'
    ichimoku_text = '✅ ICHIMOKU' if ichimoku else '❌ ICHIMOKU'
    macd_text = '✅ MACD' if macd else '❌ MACD'
    psar_text = '✅ PSAR' if psar else '❌ PSAR'
    stc_text = '✅ STOCHASTIC' if stochastic else '❌ STOCHASTIC'
    blng_text = '✅ BOLLINGER' if bollinger else '❌ BOLLINGER'

    
    keyboard = {
        "inline_keyboard":[
        [{"text":rsi_text,"callback_data":f"indc:rsi:{str(rsi)}"},{"text":cci_text,"callback_data":f"indc:cci:{str(cci)}"}],
        [{"text":ema_text,"callback_data":f"indc:ema:{str(ema)}"},{"text":psar_text,"callback_data":f"indc:psar:{str(psar)}"}],
        [{"text":ichimoku_text,"callback_data":f"indc:ichimoku:{str(ichimoku)}"},{"text":macd_text,"callback_data":f"indc:macd:{str(macd)}"}],
        [{"text":blng_text,"callback_data":f"indc:bollinger:{str(bollinger)}"},{"text":stc_text,"callback_data":f"indc:stochastic:{str(stochastic)}"}],
        [{"text":"⏱ تایم فریم ها","callback_data":"timeframes"}],
        [{'text':'💸 خرید و فروش','callback_data':'buynsell'}]]}
    
    text = '''❌اندیکاتور غیرفعال
✅ اندریکاتور فعال
'''
    if callback is None:
        sender.SendMessage(data.chatid,text,keyboard)
    else:
        sender.EditMessage(data.chatid,data.message_id,text,keyboard)


@users.vip_permission
def user_basket(data):
    find_data = db.watchlist.find_one({'chatid':data.chatid})
    if find_data is None:
        db.watchlist.insert_one({'chatid':data.chatid,'buy':True,'sell':True,
        '15m':True,'1h':True,'4h':True,'1d':True,'currencies':[]})
    sender.SendMessage(data.chatid,persian.basket_welcome,basket_keyboard)


def payment_thanks(chatid,verify_json):
    track_id = verify_json['track_id']
    amount = verify_json['payment']['amount']
    text = persian.user_pay_thanks.format(track_id,amount)
    sender.SendMessage(chatid,text)


@users.vip_permission
def user_portfo(data):
    count = db.portfo.count({'chatid':data.chatid,'exit':False})
    text = persian.portfo_welcome.format(str(count))
    sender.SendMessage(data.chatid,text,portfo_keyboard)


def admin_run(data,edit=False):
    text = '👇 یکی از گزینه های زیر را انتخاب کنید'
    if edit != False :
        sender.EditMessage(data.chatid,data.chatid,text)
    else:
        sender.SendMessage(data.chatid,text,keyboard=admin_inline_keyboard)


def admin_contact(data):
    chatid = data.text.split('_')[1]
    message_id = data.text.split('_')[1]
    text = 'پیام را ارسال کنید'
    db.update_stat(data.chatid,f'adminct:{chatid}:{message_id}')
    sender.SendMessage(data.chatid,text,return_keyboard)


@users.vip_permission
def user_signal_vip(data):
    find_data = db.vip_signals.find_one({'chatid':data.chatid})
    if find_data is not None:
        status = find_data['status'] or False
        status_short = '❌ غیرفعال' if status == False else '✅ فعال'
        status_text = '✅ فعال سازی' if status == False else '❌ غیرفعال'
    else:
        status = False
        status_short = '❌ غیرفعال'
        status_text = '✅ فعال سازی'
    text = 'برای فعال سازی یا غیرفعال سازی انتخاب کنید 👇' + f'\nوضعیت : {status_short}'
    keyboard = {'inline_keyboard':[[{'text':status_text,'callback_data':f'signalvip:{str(status)}'}]]}
    sender.SendMessage(data.chatid,text,keyboard)