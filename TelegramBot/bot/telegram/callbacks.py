from bson import ObjectId
from src.binance.market import ticker_24hr
from src.requests.telegram import EditMessage, SendMessage , RemoveMessage , EditMessage
from ..keyboards.keyboards import return_keyboard,admin_keyboard
from ..keyboards.inline_keyboards import subs_keyboard
from ..texts import persian
from src.models.mongodb import db
from ..telegram import messages
import time



def callback_check_price(data):
    symbol = data.callback_data[4:]
    ticker_data = ticker_24hr(symbol)
    if ticker_data is not None:
        text = persian.ticker24_price.format(ticker_data['symbol'],ticker_data['price_change'],
        ticker_data['price_change_percent'],ticker_data['last_price'],ticker_data['high_price'])
        keyboard = {'inline_keyboard':[[{'text':'↩️ بروزرسانی ','callback_data':'rch:'+symbol}]]}
    else:
        text = '❗️اطلاعاتی یافت نشد'
        keyboard = None
    EditMessage(data.chatid,data.message_id,text,keyboard=keyboard)


def callback_indicators_update(data):
    splited = data.callback_data.split(':')
    text = splited[1]
    stat = splited[2]
    if stat == 'False' :
            stat = True
    else :
            stat = False

    db.indicators.update_one({'chatid':data.chatid},{'$set':{text:stat}})
    messages.signal_andicator(data,callback=True)


def callback_timeframes(data,callback_text='tmfr'):
    text = 'لطفا یکی از تایم فریم های زیر را انتخاب کنید 👇'
    return_text = ''
    if callback_text == 'tmfr' :
        return_text += 'returninc'
        user = db.indicators.find_one({'chatid':data.chatid})
    elif callback_text == 'sifr' :
        return_text += 'returnsigs'
        user = db.strategies.find_one({'chatid':data.chatid})
    resolutions = user['resolutions']
    m15 = '15 دقیقه ای' + ' ❌ '
    h1 = '1 ساعته' + ' ❌ '
    h4 = '4 ساعته' + ' ❌ '
    d1 = '1 روزه'  + ' ❌ '
    if '15m' in resolutions :  m15 = '15 دقیقه ای' + ' ✅ '
    if '1h'  in resolutions :  h1  = '1 ساعته' + ' ✅ '
    if '4h'  in resolutions :  h4 = '4 ساعته' + ' ✅ '
    if '1d'  in resolutions :  d1 = '1 روزه'  + ' ✅ '
    
    keyboard = {'inline_keyboard':[
    [{'text':m15,'callback_data':f'{callback_text}:15m'}],
    [{'text':h1,'callback_data':f'{callback_text}:1h'}],
    [{'text':h4,'callback_data':f'{callback_text}:4h'}],
    [{'text':d1,'callback_data':f'{callback_text}:1d'}],
    [{'text':'↩️ بازگشت','callback_data':return_text}]]}

    EditMessage(data.chatid,data.message_id,text,keyboard)



def callback_timeframes_update(data,callback_text):
    timeframe = data.callback_data[5:]
    if callback_text == 'tmfr' : collection = db.indicators
    elif callback_text == 'sifr' : collection = db.strategies
    user = collection.find_one({'chatid':data.chatid})
    resolutions = user['resolutions']
    if timeframe in resolutions:
        resolutions.remove(timeframe)
    else:
        resolutions.append(timeframe)
    collection.update_one({'chatid':data.chatid},{'$set':{'resolutions':resolutions}})
    callback_timeframes(data,callback_text)


def callback_strategies_update(data):
    splited = data.callback_data.split(':')
    text = splited[1]
    stat = splited[2]
    if stat == 'False' :
            stat = True
    else :
            stat = False

    db.strategies.update_one({'chatid':data.chatid},{'$set':{text:stat}})
    messages.signal_strategy(data,callback=True)


def callback_signals_timeframes_update(data):
    timeframe = data.callback_data[5:]
    user = db.strategies.find_one({'chatid':data.chatid})
    resolutions = user['resolutions']
    if timeframe in resolutions:
        resolutions.remove(timeframe)
    else:
        resolutions.append(timeframe)
    db.strategies.update_one({'chatid':data.chatid},{'$set':{'resolutions':resolutions}})
    callback_timeframes(data,'sifr')


def callback_watchlist_add(data):
    text = 'نماد مورد نظر خود را ارسال کنید 👇'
    db.update_stat(data.chatid,'watchlist')
    RemoveMessage(data.chatid,data.message_id)
    SendMessage(data.chatid,text,return_keyboard)


def callback_watchlist_timeframe(data):
    text = 'select time frame'
    find_data = db.watchlist.find_one({'chatid':data.chatid})
    if find_data is None:
        fifteen_min = True
        one_hour = True
        four_hour = True
        one_day = True
        db.watchlist.insert_one({'chatid':data.chatid,'buy':True,'sell':True,
        '15m':fifteen_min,'1h':one_hour,'4h':four_hour,'1d':one_day,'currencies':[]})
    else:
        fifteen_min = find_data['15m']
        one_hour = find_data['1h']
        four_hour = find_data['4h']
        one_day = find_data['1d']

    fifteen_min_text = '✅ 15 دقیقه' if fifteen_min else '❌ 15 دقیقه'
    one_hour_text = '✅ 1 ساعت' if one_hour else '❌ 1 ساعت'
    four_hour_text = '✅ 4 ساعت' if four_hour else '❌ 4 ساعت'
    one_day_text = '✅ 1 روز' if one_day else '❌ 1 روز'


    keyboard = {"inline_keyboard":[
    [{"text":fifteen_min_text,"callback_data": 'bsktime:15m:' + str(fifteen_min)},
    {"text":one_hour_text,"callback_data": 'bsktime:1h:' + str(one_hour)}],
    [{"text":four_hour_text,"callback_data": 'bsktime:4h:' + str(four_hour)},
    {"text":one_day_text,"callback_data":"bsktime:1d:" + str(one_day)}]]}
    EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_update_watchlist_time(data):
    splited = data.callback_data.split(':')
    text = splited[1]
    stat = splited[2]
    if stat == 'False' :
            stat = True
    else :
            stat = False
    
    db.watchlist.update_one({'chatid':data.chatid},{'$set':{text:stat}})
    callback_watchlist_timeframe(data)


def callback_watchlist_add_db(data):
    splited = data.callback_data.split(':')
    symbol = splited[1]
    find_user = db.watchlist.find_one({'chatid':data.chatid})
    try:
        currencies = find_user['currencies']
    except:
        currencies = []
        if find_user is None:
            db.watchlist.insert_one({'chatid':data.chatid,'buy':True,'sell':True,
            '15m':True,'1h':True,'4h':True,'1d':True,'currencies':[]})
    if symbol not in currencies:
        if len(currencies) >= 10:
            text = '❌ متاسفانه تعداد ارزهای شما بیشتر از حد مجاز میباشد!'
        else:
            text = '✅ ارز مورد نظر با موفقیت به واچ لیست اضافه شد!'
            db.watchlist.update_one({'chatid':data.chatid},{'$push':{'currencies':symbol}})
    else:
        text = '❌ متاسفانه ارز مورد نظر شما تکراری می باشد!'
    

    EditMessage(data.chatid,data.message_id,text)
    messages.user_start(data)


def callback_watchlist_remove(data):
    find_user = db.watchlist.find_one({'chatid':data.chatid})
    currencies = find_user['currencies']
    if find_user is None or len(currencies) == 0:
        text = '❌ متاسفانه واچ لیست شما خالی می باشد.'
        EditMessage(data.chatid,data.message_id,text)
    else:
        currencies = find_user['currencies']
        text = '👇 برای حذف ارز مورد نظر خود از واچ لیست انتخاب کنید'
        keyboard = []
        for symbol in currencies:
            callback_text = '🔸 ' + symbol + ' 🔸'
            keyboard.append([{'text':callback_text,'callback_data':'rwatch:'+symbol}])
        
        keyboard = {'inline_keyboard':keyboard}
        EditMessage(data.chatid,data.message_id,text,keyboard)



def callback_watchlist_remove_db(data):
    splited = data.callback_data.split(':')
    symbol = splited[1]
    db.watchlist.update_one({'chatid':data.chatid},{'$pull':{'currencies':symbol}})
    text = '✅ ارز مورد نظر با موفقیت حذف شد!'
    EditMessage(data.chatid,data.message_id,text)


def callback_portfo_setstat(data):
    db.update_stat(data.chatid,'portfoadd')
    text = '👇 ارز مورد نظر خود را وارد کنید.'
    RemoveMessage(data.chatid,data.message_id)
    SendMessage(data.chatid,text,return_keyboard)


def callback_portfo_add(data):
    symbol = data.callback_data[8:]
    db.update_stat(data.chatid,f'portfoup:{symbol}')
    text = '👇قیمت خرید خود را وارد کنید  (USDT تتر)'
    RemoveMessage(data.chatid,data.message_id)
    SendMessage(data.chatid,text,return_keyboard)


def callback_portfo_me(data):
    user_portfo = db.portfo.find({'chatid':data.chatid,'exit':False})
    if user_portfo.count() == 0:
        text = ' ارزی در پورتفو شما یافت نشد❗️'
        keyboard = {'inline_keyboard':[[{"text":"➕ افزودن ارز","callback_data":"prtadd"}]]}
    else:
        text = '⭐️ برای دریافت اطلاعات بیشتر ارز مورد نظر خود را انتخاب کنید  👇'
        keyboard = []
        for profile in user_portfo:
            symbol = profile['symbol']
            id = str(profile['_id'])
            keyboard.append([{'text': f'⚡️ {symbol} ⚡️' , 'callback_data': 'prtf:' + id}])
        
        keyboard = {'inline_keyboard':keyboard}
    EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_portfo_analyze(data):
    portfo = db.portfo.find({'chatid':data.chatid,'exit':False})
    portfo_exit = db.portfo.find({'chatid':data.chatid,'exit':True})
    total_profit_count = 0
    total_loss_count = 0
    total_profit_prs = 0
    total_loss_prs = 0
    total_profit = 0
    total_loss = 0
    bating_avg = 0
    win_avg = 0
    loss_avg = 0
    win_loss_ratio = 0

    for stock_exit in portfo_exit:
        symbol = stock_exit['symbol']
        stock_price = stock_exit['sell']
        user_price = stock_exit['buy']
        user_vol = stock_exit['volume']
        user_final_price = float(user_vol) * float(user_price)
        stock_final_price = float(user_vol) * float(stock_price)
        stock_final_price = float(stock_final_price - ((1 * stock_final_price) / 100)) # one percent has been applied on price
        user_final_price = float( user_final_price - ((1 * user_final_price) / 100)) # one percent has been applied on price
        profit_cash_exit = stock_final_price - user_final_price
        profit_exit = round(( profit_cash_exit / user_final_price ) * 100 , 2)
        if profit_exit > 0 : total_profit_count += 1 ; total_profit_prs += profit_exit ; total_profit += profit_cash_exit
        if profit_exit < 0 : total_loss_count += 1 ; total_loss_prs += profit_exit ; total_loss += profit_cash_exit
        if portfo_exit.count() > 0 : bating_avg = total_profit_count / portfo_exit.count()
        if total_profit_count > 0 : win_avg = total_profit_prs / total_profit_count
        if total_loss_count > 0 : loss_avg = ( total_loss_prs / total_loss_count ) * -1
        if loss_avg != 0 : win_loss_ratio = win_avg / loss_avg ; win_loss_ratio = round(win_loss_ratio , 2)

    for stock in portfo:
        symbol = stock['symbol']
        stock_price = stock['sell']
        stock_detail = ticker_24hr(symbol)
        stock_price = stock_detail['last_price']
        user_price = stock['buy']
        user_vol = stock['volume']
        user_final_price = float(user_vol) * float(user_price)
        stock_final_price = float(user_vol) * float(stock_price)
        stock_final_price = float(stock_final_price - ((1 * stock_final_price) / 100)) # one percent has been applied on price
        user_final_price = float( user_final_price - ((1 * user_final_price) / 100)) # one percent has been applied on price
        profit_cash = stock_final_price - user_final_price
        profit = round(( profit_cash / user_final_price ) * 100 , 2)
        if profit > 0 : total_profit_count += 1 ; total_profit_prs += profit ; total_profit += profit_cash
        if profit < 0 : total_loss_count += 1 ; total_loss_prs += profit ; total_loss += profit_cash
        if portfo.count() > 0 : bating_avg = total_profit_count / portfo.count()
        if total_profit_count > 0 : win_avg = total_profit_prs / total_profit_count
        if total_loss_count > 0 : loss_avg = ( total_loss_prs / total_loss_count ) * -1
        if loss_avg != 0 : win_loss_ratio = win_avg / loss_avg ; win_loss_ratio = round(win_loss_ratio , 2)

    text = persian.portfo_analyze.format(total_profit_count,total_loss_count,total_profit_prs,
    total_loss_prs,total_profit,total_loss,bating_avg,win_avg,float(loss_avg),win_loss_ratio)
    EditMessage(data.chatid,data.message_id,text)


def callback_portfo_detail(data):
    id = data.callback_data[5:]
    portfo = db.portfo.find_one({'chatid':data.chatid,'_id':ObjectId(id)})
    if portfo is not None:
        portfo = db.portfo.find_one({'_id':ObjectId(id),'chatid':data.chatid,'exit':False})
        symbol = portfo['symbol']
        stock = ticker_24hr(symbol)
        stock_price = stock['last_price']
        name = symbol
        user_price = portfo['buy']
        user_vol = portfo['volume']
        stock_final_price = float(stock_price) * user_vol
        stock_final_price = float(stock_final_price - ((1 * stock_final_price) / 100)) # one percent has been applied on price
        user_final_price = float(user_vol) * float(user_price)
        user_final_price = float( user_final_price - ((1 * user_final_price) / 100)) # one percent has been applied on price
        profit = round(((stock_final_price - user_final_price ) / user_final_price ) * 100 , 4)
        if profit > 0 : profit_text = '📈 درصد سود : ' + str(profit) + ' %'
        if profit < 0 : profit_text = '📉 درصد ضرر : ' + str(profit) + ' %'
        text = persian.portfo_single.format(name,stock_price,stock_final_price,user_price,user_vol,user_final_price,profit_text)
        
        keyboard = {'inline_keyboard':[[{'text':'↩️ خروج','callback_data':'prte:' + str(id)}],
        [{'text':'🔁 آپدیت','callback_data':'prtf:' + id}]]}
    else:
        text = '⚠️ سهم مورد نظر از لیست حذف شده است'
        keyboard = {'inline_keyboard':[[{"text":"👨🏻‍💻 ارزهای من","callback_data":"prtme"}]]}

    EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_portfo_exit(data):
    id = data.callback_data[5:]
    portfo = db.portfo.find_one({'_id':ObjectId(id),'exit':False})
    if portfo is not None:
        db.update_stat(data.chatid,f'prte:{id}')
        text = 'قیمت فروش ارز خود را وارد کنید 👇'
        RemoveMessage(data.chatid,data.message_id)
        SendMessage(data.chatid,text,return_keyboard)
    else:
        text = 'شما قبلا ارز خود را از لیست خارج کرده اید ❗️'
        EditMessage(data.chatid,data.message_id,text)


def callback_buynsell(data,callback_text='sbns'):
    text = 'انتخاب کنید'
    find_data = db.indicators.find_one({'chatid':data.chatid})
    return_text = ''
    if callback_text == 'sbns' :
        return_text += 'returninc'
        user = db.indicators.find_one({'chatid':data.chatid})
        callback = 'checkbns'
    elif callback_text == 'sifr' :
        return_text += 'returnsigs'
        user = db.strategies.find_one({'chatid':data.chatid})
        callback = 'scheckbns'
    
    buy = user['buy']
    sell = user['sell']
    buy_text = '✅ خرید' if buy else '❌ خرید'
    sell_text = '✅ فروش' if sell else '❌ فروش'
    keyboard  = {"inline_keyboard":[
    [{"text":buy_text,"callback_data":f"{callback}:buy:{str(buy)}"},
    {"text":sell_text,"callback_data":f"{callback}:sell:{str(sell)}"}],
    [{'text':'↩️ بازگشت','callback_data':return_text}]]}
    EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_bns_update(data,callback_text='sbns'):
    splited = data.callback_data.split(':')
    text = splited[1]
    stat = splited[2]
    if callback_text == 'sbns' : collection = db.indicators
    else : collection = db.strategies
    if stat == 'False' :
            stat = True
    else :
            stat = False
    collection.update_one({'chatid':data.chatid},{'$set':{text:stat}})
    callback_buynsell(data,callback_text)


def callback_admin_statistics(data):
    admin_count = str(db.admins.count())
    members_count = str(db.members.count())
    vips_count = str(db.vip.count({'expire':{'$gt':time.time()}}))
    text = persian.admin_stats.format(vips_count,members_count,admin_count)
    EditMessage(data.chatid,data.message_id,text)


def callback_admin_lists(data):
    text = 'برای اعمال تغییرات یکی از ادمین های زیر را انتخاب کنید'
    keyboard = []
    admins = db.admins.find()
    for admin in admins:
        name = admin['name']
        id = str(admin['_id'])
        keyboard.append([{'text':name,'callback_data': 'adch:' + id}])
    keyboard = {'inline_keyboard':keyboard}
    EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_admin_setting(data):
    id = data.callback_data[5:]
    admin = db.admins.find_one({'_id':ObjectId(id)})
    admin_chatid = admin['chatid']
    if admin_chatid == data.chatid:
        text = 'خطا : امکان اعمال تغییرات وجود ندارد!'
        EditMessage(data.chatid,data.message_id,text)
    else:
        text = 'انتخاب کنید'
        keyboard = {"inline_keyboard":[
            [{"text":"🗑 حذف","callback_data":"addel:" + id}],
            [{"text":"👨‍💻 تغییر دسترسی","callback_data":"adprv:" + id}]]}
        EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_admin_prvs(data,chid=None):
    admin = db.admins.find_one({'chatid':data.chatid})
    prvs = admin['privileges']
    master = prvs['master']
    if master:
        text = 'برای تغییر دسترسی انتخاب کنید'
        if chid is None:
            id = data.callback_data[6:]
            filter = {'_id':ObjectId(id)}
        else : filter = {'chatid':chid}
        target = db.admins.find_one(filter)
        target_chatid = target['chatid']
        target_prvs = target['privileges']
        target_master = target_prvs['master']
        text += f'\nmaster : {str(target_master)}'
        receive_message = target_prvs['receive_message']
        price_update = target_prvs['price_update']
        rcv_text = '✅ دریافت پیام' if receive_message else '❌ دریافت پیام'
        prc_text = '✅ تغییر قیمت' if price_update else '❌ تغییر قیمت'
        keyboard = {'inline_keyboard':[
            [{'text':rcv_text,'callback_data':f'adprs:receive_message:{str(receive_message)}:{str(target_chatid)}' }],
            [{'text':prc_text,'callback_data':f'adprs:price_update:{str(price_update)}:{str(target_chatid)}'}]]}
        EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_admin_prvs_update(data):
    splited = data.callback_data.split(':')
    text = splited[1]
    stat = splited[2]
    chatid = int(splited[3])    
    db.admins.update_one({'chatid':chatid},{'$set':{f'privileges.{text}':stat}})
    callback_admin_prvs(data,chatid)


def callback_admin_del(data):
    admin = db.admins.find_one({'chatid':data.chatid})
    prvs = admin['privileges']
    master = prvs['master']
    if master:
        id = data.callback_data[6:]
        db.admins.remove({'_id':ObjectId(id)})
        text = 'ادمین با موفقیت حذف شد'
    else:
        text = '403'
    EditMessage(data.chatid,data.message_id,text)


def callback_admin_create(data):
    admin = db.admins.find_one({'chatid':data.chatid})
    prvs = admin['privileges']
    master = prvs['master']
    if master:
        text = '👇 ابتدا چت آیدی موردنظر خود را وارد کنید'
        db.update_stat(data.chatid,'createadmin')
        RemoveMessage(data.chatid,data.message_id)
        SendMessage(data.chatid,text,return_keyboard)
    else:
        text = '403'
        EditMessage(data.chatid,data.message_id,text)


def callbacks_admin_insert(data):
    chatid = data.callback_data[9:]
    chatid = int(chatid)
    member = db.members.find_one({'chatid':chatid})
    first_name = member['first_name']
    db.admins.insert_one({'chatid':chatid,'name':first_name,'privileges':
    {'receive_message':True,'price_update':True,'master':False}})
    text = 'انجام شد'
    text2 = 'دسترسی شما به ادمین تغییر کرد'
    EditMessage(data.chatid,data.message_id,text)
    SendMessage(chatid,text2,admin_keyboard)


def callback_admin_subs(data):
    admin = db.admins.find_one({'chatid':data.chatid})
    prvlg = admin['privileges']
    price = prvlg['price_update']
    master = prvlg['master']
    if price or master:
        text = 'انتخاب کنید'
        EditMessage(data.chatid,data.message_id,text,subs_keyboard)


def callback_admin_subs_crplan(data):
    RemoveMessage(data.chatid,data.message_id)
    text = 'تعداد روز اشتراک مورد نظر را به صورت عدد وارد کنید . مثلا 30'
    db.update_stat(data.chatid,'createplan')
    SendMessage(data.chatid,text,return_keyboard)


def callback_admin_subs_plans(data):
    RemoveMessage(data.chatid,data.message_id)
    keyboard = []
    plans = db.plans.find()
    for plan in plans:
        id = str(plan['_id'])
        description = plan['description']
        keyboard.append([{'text':description,'callback_data':'admch:' + id}])
    keyboard = {'inline_keyboard':keyboard}
    text = 'برای اعمال تغییرات یکی از گزینه های زیر را انتخاب کنید'
    SendMessage(data.chatid,text,keyboard)


def callback_admin_subs_planshow(data):
    print(data.callback_data)
    id = data.callback_data[6:]
    try:
        plan = db.plans.find_one({'_id':ObjectId(id)})
        days = str(plan['days'])
        price = str(plan['price'])
        description = str(plan['description'])
        keyboard = {'inline_keyboard':[[
            {'text':'🗑 حذف اشتراک','callback_data':f'adplndel:{id}'},
            {'text':'✍️ ویرایش','callback_data':f'adplned:{id}'}]]}
        text = 'روز : {} \nقیمت : {}\nاطلاعات : {}'.format(days,price,description)
        EditMessage(data.chatid,data.message_id,text,keyboard)
    except Exception as e:
        text = 'مشکلی پیش اومد :('
        SendMessage(data.chatid,text)


def callback_admin_subs_plandel(data):
    count = db.plans.count()
    if count < 2:
        text = 'تعداد اشتراک ها نمیتواند کمتر از 2 عدد باشد'
    else:
        id = data.callback_data[9:]
        db.plans.remove({'_id':ObjectId(id)})
        text = 'اشتراک مورد نظر با موفقیت حذف شد'
    EditMessage(data.chatid,data.message_id,text)


def callback_admin_subs_planedit(data):
    RemoveMessage(data.chatid,data.message_id)
    id = data.callback_data[8:]
    db.update_stat(data.chatid,f'edtpln:{id}')
    text = 'تعداد روز اشتراک مورد نظر را به صورت عدد وارد کنید . مثلا 30'
    SendMessage(data.chatid,text,return_keyboard)


def callback_admin_subs_discount(data):
    admin = db.admins.find_one({'chatid':data.chatid})
    prvlg = admin['privileges']
    price = prvlg['price_update']
    discount_rule = db.rules.find_one({'rule':'discount'})
    #check prvlg later
    if discount_rule is not None:
        text = '✅ تخفیفی اعمال شده'
        callback_data = 'rmvdiscount'
        callback_text = '➖ حذف تخفیف'
        
    else:
        text = '🚩 تخفیفی اعمال نشده' 
        callback_data = 'crdiscount'
        callback_text = '➕ اعمال تخفیف'
    keyboard = {'inline_keyboard':[[{'text':callback_text,'callback_data':callback_data}]]}
    EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_admin_subs_discount_add(data):
    text = 'درصد تخفیف را وارد کنید! مثلا : 20'
    db.update_stat(data.chatid,'crdiscount')
    RemoveMessage(data.chatid,data.message_id)
    SendMessage(data.chatid,text,return_keyboard)


def callback_admin_subs_discount_rmv(data):
    text = 'تخفیف با موفقیت حذف شد!'
    rule = db.rules.find_one({'rule':'discount'})
    db.rules.remove({'rule':'discount'})
    if rule is not None:
        last_plan = rule['plans']
        for plan in last_plan:
            price = plan[0]['price']
            id = str(plan[0]['_id'])
            db.plans.update_one({'_id':ObjectId(id)},{'$set':{'price':price}})
        EditMessage(data.chatid,data.message_id,text)


def callback_admin_channels(data):
    keyboard = []
    channels = db.channels.find()
    if channels is not None:
        text =  'برای حذف کانال انتخاب کنید'
        for channel in channels:
            id = str(channel['_id'])
            channel_name = channel['details']
            keyboard.append([{'text':channel_name,'callback_data':f'adrmvh:{id}'}])
    else:
        text = 'کانالی یافت نشد'
    keyboard.append([{'text':'➕ اضافه کردن کانال','callback_data':f'adcrch'}])
    keyboard = {'inline_keyboard':keyboard}
    EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_admin_channels_create(data):
    text = 'ابتدا چنل آیدی را وارد کنید'
    db.update_stat(data.chatid,'chcreate')
    RemoveMessage(data.chatid,data.message_id)
    SendMessage(data.chatid,text,return_keyboard)


def callback_admin_channels_rmv(data):
    id = str(data.callback_data[7:])
    db.channels.remove({'_id':ObjectId(id)})
    text = 'کانال با موفقیت حذف شد'
    EditMessage(data.chatid,data.message_id,text)


def callback_admin_subs_free(data):
    text = 'چند ساعت اشتراک رایگان باشه؟'
    db.update_stat(data.chatid,'adminfreevip')
    RemoveMessage(data.chatid,data.message_id)
    SendMessage(data.chatid,text,return_keyboard)


def callback_user_signalvip(data):
    splited = data.callback_data.split(':')
    stat = splited[1]
    if stat == 'False' :
            stat = True
    else :
            stat = False
    print(stat)
    db.vip_signals.update({'chatid':data.chatid},{'$set':{'status':stat}},upsert=True)
    status_short = '✅ فعال شد' if stat else '❌ غیرفعال شد'
    status_text = '✅ فعال سازی' if stat == False else '❌ غیرفعال'
    text = f'دریافت سیگنال با موفقیت {status_short}'
    keyboard = {'inline_keyboard':[[{'text':status_text,'callback_data':f'signalvip:{str(stat)}'}]]}
    EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_admin_users_check(data):
    text = '👇 چت آیدی را وارد کنید'
    db.update_stat(data.chatid,'aduser')
    RemoveMessage(data.chatid,data.message_id)
    SendMessage(data.chatid,text,return_keyboard)


def callback_admin_tovip(data):
    chatid = data.callback_data.split(':')[1]
    text = 'تعداد روز مد نظر خود را وارد کنید'
    db.update_stat(data.chatid,f'updatevip:{str(chatid)}')
    keyboard = {'inline_keyboard':[[{'text':'😡 حذف اشتراک','callback_data':f'removevip:{str(chatid)}'}]]}
    EditMessage(data.chatid,data.message_id,text,keyboard)


def callback_admin_removevip(data):
    chatid = int(data.callback_data.split(':')[1])
    user = db.vip.find_one({'chatid':chatid})
    if user is not None:
        db.vip.remove({'chatid':chatid})
        text = 'اشتراک حذف شد'
    else:
        text = 'کاربر اشتراک ندارد'
    SendMessage(chatid,text)


def callback_admin_userinfo(data):
    text = ''
    chatid = int(data.callback_data.split(':')[1])
    print(chatid)
    user = db.members.find_one({'chatid':chatid})
    name = user['first_name']
    vip = db.vip.find_one({'chatid':chatid})
    text += 'نام :' + str(name) + '\n' + 'چت آیدی : ' + str(chatid) + '\n'
    if vip is not None:
        text += 'اشتراک : دارد'
    else:
        text += 'اشتراک : ندارد'
    
    SendMessage(data.chatid,text)
    
