import dotenv
import os

DEBUG = os.getenv('DEBUG')
if DEBUG is None : DEBUG = True ; dotenv.load_dotenv()
else : DEBUG = False
MONGODB = os.getenv('MONGODB')
TOKEN = os.getenv('TOKEN')
URL = f'https://api.telegram.org/bot{TOKEN}/'
ADMINS = [98056633]
BOT_USERNAME = os.getenv('BOT_USERNAME')
BOT_USERNAME_AT = '@' + BOT_USERNAME
CHANNEL_ID = os.getenv('CHANNEL_ID')
CHANNEL_URL = os.getenv('CHANNEL_URL')
IDPAY_TOKEN = os.getenv('IDPAY_TOKEN')
WEB_URL = os.getenv('WEB_URL')
SENTRY_DSN = os.getenv('SENTRY_DSN')
