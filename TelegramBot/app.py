from flask import Flask
from init import init
import config
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

sentry_sdk.init(
    dsn=config.SENTRY_DSN,
    integrations=[FlaskIntegration()],
    traces_sample_rate=5.0
)


app = Flask(__name__)


from bot.bot import telegram
app.register_blueprint(telegram)

from payments.pay import payment
app.register_blueprint(payment)

if __name__ == '__main__':
    init.start()
    app.run(port=5000,debug=config.DEBUG)