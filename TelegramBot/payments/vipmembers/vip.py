from src.models.mongodb import db
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time


class vip_member:
    def __init__(self, chatid):
        self.expire = None
        self.chatid = None
        self.is_vip = False
        user = db.vip.find_one({'chatid': chatid})
        if user is not None:
            self.expire = user['expire']
            self.chatid = user['chatid']
            unixtime = datetime.now().timestamp()
            if unixtime > self.expire:
                self.is_vip = False
            if self.expire > unixtime:
                self.is_vip = True
    
    def create_vip(self,chatid,exptimestamp):
        find = db.vip.count_documents({'chatid':chatid})
        if find == 0:
            db.vip.insert_one({'chatid':chatid,'expire':exptimestamp})
        if find == 1:
            db.vip.update_one({'chatid':chatid},{ "$set": { "expire": exptimestamp }})





def user_to_vip(chatid, amount):
    days = db.plans.find_one({'price':amount})['days']
    user = vip_member(chatid)
    if user.is_vip:
        date = datetime.fromtimestamp(user.expire)
    else:
        date = datetime.now()

    exptime = date + relativedelta(days=days)
    expireunix = time.mktime(exptime.timetuple())
    user.create_vip(chatid,expireunix)


def user_to_vip_day(chatid,day):
    user = vip_member(chatid)
    if user.is_vip:
        date = datetime.fromtimestamp(user.expire)
    else:
        date = datetime.now()
    print(date)
    exptime = date + relativedelta(days=day)
    expireunix = time.mktime(exptime.timetuple())
    print('expire ',expireunix)
    print('chatid ',chatid)
    user.create_vip(chatid,expireunix)


