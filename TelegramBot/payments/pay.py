import json
from flask import (Blueprint, jsonify, redirect, request,render_template)
from src.models.mongodb import db
from bson import ObjectId
from .idpay import idpay
import config

payment = Blueprint('payment', __name__, url_prefix='/pay',template_folder='template')


@payment.route('/generate/<int:price>/<path:id>', methods=['GET'])
def generate(price, id):
    try:
        id = ObjectId(id)
    except:
        return jsonify({'error': 'invalid id'}), 400

    find_user = db.members.find_one({'_id': id})
    if find_user is None:
        return jsonify({'error': 'user not found'}), 404

    chatid = find_user['chatid']
    generated_link = idpay.payments.gen_pay_link(price, chatid)

    return redirect(generated_link)


@payment.route('/callback', methods=['POST'])
def callback():
    back = dict(request.form)
    payed = idpay.payments.verify(back)
    track_id = back['track_id']
    bot_link = f'https://t.me/{config.BOT_USERNAME}'
    if payed:
        amount = back['amount']
        return render_template('success.html',
        track_id=track_id,bot_link=bot_link,amount=amount)
    else:
        return render_template('failed.html',
        track_id=track_id,bot_link=bot_link)
        return 'FAILED'
