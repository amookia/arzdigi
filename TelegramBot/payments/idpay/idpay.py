import requests
from src.models.mongodb import db
import random
import config
from ..vipmembers import vip
from bot.telegram import messages


class payments:
    def __init__(self):
        self.api_key = config.IDPAY_TOKEN
        # Sandbox is on (test) Turn it off 'X-SANDBOX':'1' --> testmode
        self.header = {'X-API-KEY': self.api_key}
        self.callback = 'https://tahlilgarbot.w4y.ir' + '/pay/callback'

    def is_payed(self, id):
        transaction = db.transaction.find_one({'id': id})
        return transaction['payed']

    def gen_pay_link(self, price, chatid):
        plan = db.plans.find_one({'price':price})
        if plan is not None:
            rand = random.randint(100, 99999)
            url = 'https://api.idpay.ir/v1.1/payment'
            datas = {'order_id': str(rand), 'amount': price,
                    'callback': self.callback}
            response = requests.post(url, json=datas, headers=self.header).json()
            id = response['id']
            link = response['link']
            db.transaction.insert_one({'id': id, 'link': link, 'payed': False, 'chatid': chatid,
                                    'order_id': rand, 'details': {}})
            return response['link']
        else:
            return 'https://t.me/' + config.BOT_USERNAME_AT

    def verify(self, details):
        status = details['status']
        id = details['id']
        order_id = details['order_id']
        url = 'https://api.idpay.ir/v1.1/payment/verify'
        datas = {'id': id, 'order_id': order_id}
        verifytext = requests.post(url, json=datas, headers=self.header)
        verifyjson = verifytext.json()
        try:
            amount = int(verifyjson['amount'])
        except:
            return False
        if verifytext.status_code == 200 and \
                verifyjson['status'] == 100 and self.is_payed(id) != True and status == '10':
            db.transaction.update_one(
                {'id': id}, {'$set': {'details': details, 'payed': True}})
            chatid = db.transaction.find_one({'id': id})['chatid']
            vip.user_to_vip(chatid,amount)
            # TODO send thanks message to user
            messages.payment_thanks(chatid,verifyjson)
            return True
        else:
            return False


payments = payments()
