from enum import unique
from pymongo import MongoClient
import config
import ssl


class DB:
    def __init__(self):
        client = MongoClient(config.MONGODB,ssl_cert_reqs=ssl.CERT_NONE)
        db = client['currencyBot']
        self.symbols = db['symbols']
        self.members = db['members']
        self.contacts = db['contacts']
        self.transaction = db['transaction']
        self.plans = db['plans']
        self.vip = db['vip']
        self.indicators = db['user_indicators']
        self.strategies = db['signal_strategies']
        self.watchlist = db['watch_list']
        self.portfo = db['portfo']
        self.tickers = db['tickers']
        self.admins = db['admins']
        self.rules = db['rules']
        self.channels = db['channels']
        self.rates = db['rates']
        self.vip_signals = db['vip_signals']
        self.symbols.ensure_index('symbol',unique=True)
        self.contacts.ensure_index('expire', expireAfterSeconds=3*86400)
        self.plans.ensure_index('days',unique=True)
        self.strategies.ensure_index('chatid',unique=True)
        self.watchlist.ensure_index('chatid',unique=True)
        self.admins.ensure_index('chatid',unique=True)
        self.rules.ensure_index('rule',unique=True)
        self.vip_signals.ensure_index('chatid',unique=True)


    def update_stat(self, chatid: int, stat: str) -> None:
        db.members.update_one({'chatid': chatid}, {'$set': {'stat': stat}})
    

    def IsAdmin(self, chatid : int) -> bool :
        return db.admins.find_one({'chatid':chatid}) is not None


db = DB()
