import requests
import config
import json

def SendMessage(chatid,text,keyboard=None,reply_to=None,parse_mode=None):
    url = config.URL + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
    if reply_to is not None : url += '&reply_to_message_id=' + str(reply_to)
    if keyboard is not None: url += '&reply_markup=' + str(json.dumps(keyboard))
    if text is not None: url += f'&parse_mode={parse_mode}'
    response_json = requests.get(url).json()

def SendAnimation(chatid,file_id,text):
    url = config.URL + 'sendAnimation?chat_id=' + str(chatid) + '&animation=' + str(file_id) + '&caption=' + text
    requests.get(url)

def SendVideo(chatid,file_id,text):
    url = config.URL + 'sendVideo?chat_id=' + str(chatid) + '&video=' + str(file_id) + '&caption=' + text
    requests.get(url)

def RemoveMessage(chatid,messageid):
    url = config.URL + 'deleteMessage?chat_id=' + str(chatid) + '&message_id=' + str(messageid)
    requests.get(url)

def EditMessage(chatid,messageid,text,keyboard=None):
    url = config.URL + 'editMessageText?chat_id=' + str(chatid) + '&message_id=' + str(messageid) + '&text=' + text
    if keyboard is not None: url += '&reply_markup=' + str(json.dumps(keyboard))
    requests.get(url)

def ForwardMessage(from_chatid,to_chatid,messageid):
    url = config.URL + 'forwardMessage?from_chat_id=' + str(from_chatid) + '&message_id=' + str(messageid) + '&chat_id=' + str(to_chatid)
    return requests.get(url).json()

def CopyMessage(to_chatid,from_chatid,messageid,reply_to_messageid=None,text=None):
    url = config.URL + 'copyMessage?from_chat_id=' + str(from_chatid) + '&message_id=' + str(messageid) + '&chat_id=' + str(to_chatid)
    if text is not None: url += '&caption=' + str(text) + '&parse_mode=Markdown'
    if reply_to_messageid is not None : url += '&reply_to_message_id=' + str(reply_to_messageid)
    print(requests.get(url).json())


def MemberJoined(chatid,userid):
    url = config.URL + 'getChatMember?chat_id=' + str(chatid) + '&user_id=' + str(userid)
    response = requests.get(url).json()
    try:
        status = response['result']['status']
        if status in ('administrator','member','creator') : return True
        else : return False
    except:
        return False
