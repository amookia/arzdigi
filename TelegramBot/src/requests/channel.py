from .telegram import MemberJoined,SendMessage
from ..models.mongodb import db

def IsJoined(chatid : int) -> bool:
    channels = db.channels.find()
    checker = True
    for channel in channels:
        channel_id = channel['channel_id']
        checker = checker & MemberJoined(channel_id,chatid)
    return checker
