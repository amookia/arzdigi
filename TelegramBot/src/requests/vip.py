from src.models.mongodb import db
from src.requests.telegram import SendMessage
import time
import config
import datetime


def IsUserVip(chatid):
    find_user = db.vip.find_one({'chatid':chatid,'expire':{'$gt':time.time()}})
    find_member = db.members.find_one({'chatid': chatid})
    find_admin = db.admins.find_one({'chatid': chatid})
    check_rules = db.rules.find_one({'rule':'freevip','expire_at':{'$gt':time.time()}})
    if find_user is not None : return True
    elif check_rules is not None : return True
    elif find_admin is not None : return True
    elif find_member is None : return False
    else :
        keyboard = []
        plans = db.plans.find()
        for plan in plans:
            user_id = str(db.members.find_one({'chatid':chatid})['_id'])
            price = str(int(plan['price']/10))
            inline_text = plan['description'] + f' {price} تومان'
            url = config.WEB_URL + '/pay/generate/' + str(plan['price']) + '/' + user_id
            keyboard.append([{"text": inline_text,'url': url}])
        keyboard = {'inline_keyboard':keyboard}
        text = '❗️کاربر گرامی برای استفاده از این بخش باید اشتراک تهیه کنید !'
        SendMessage(chatid,text,keyboard)
        
        return False


def IsUserVipCheck(chatid):
    find_user = db.vip.find_one({'chatid':chatid,'expire':{'$gt':time.time()}})
    find_member = db.members.find_one({'chatid': chatid})
    find_admin = db.admins.find_one({'chatid': chatid})
    if find_member is None : return False
    elif find_user is not None : return True
    elif find_admin is not None : return True
    else : return False

def IsUserPermitted(chatid):
    date_joined = db.members.find_one({'chatid':chatid})['_id'].generation_time
    expire_time = datetime.datetime.utcnow() - datetime.timedelta(days=1)
    expire_timestamp = expire_time.timestamp()
    if expire_timestamp > date_joined.timestamp():
        return False
    else:
        return True