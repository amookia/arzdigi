from src.models.mongodb import db


def ticker_24hr(symbol):
    symbol_data = db.tickers.find_one({'s': symbol})
    if symbol_data is None:
        return None
    else:
        symbol_data = {
            'symbol': symbol_data['s'],
            'price_change': str(round(float(symbol_data['p']), 3)),
            'price_change_percent': str(round(float(symbol_data['P']), 3)),
            'last_price': str(round(float(symbol_data['c']), 3)), 
            'high_price': str(round(float(symbol_data['h']), 3)),
            'event_time' : symbol_data['E']
        }
        return symbol_data
