from logging import fatal
import re
from pymongo import MongoClient
import config
import datetime
import time
# import ssl

class Db:
    def __init__(self):
        # client = MongoClient(config.mongo_uri,ssl_cert_reqs=ssl.CERT_NONE)
        client = MongoClient(config.mongo_uri, connect=False)
        db = client["currencyBot"]
        self.rates = db["rates"]
        self.symbols = db["tickers"]
        self.indicators = db["indicators"]
        self.baskets = db["baskets"]
        self.userportfo = db["user_portfo"]
        self.vip = db["vip"]
        self.strategySignals = db["strategy_signals"]
        self.signalStrategies = db["signal_strategies"]
        self.userIndicators = db["user_indicators"]
        self.watchList = db["watch_list"]
        self.portfo = db["portfo"]
        self.members = db["members"]
        self.rules = db["rules"]
        self.admins = db["admins"]
        self.vip_signals = db["vip_signals"]
        self.indicators.ensure_index("expire", expireAfterSeconds=96*3600)
    
    def fetchVipSignal(self, chatId):
        return self.vip_signals.find_one({"chatid": chatId})

    def fetchAdmins(self):
        admins = []
        for admin in self.admins.find({}):
            admins.append(admin["chatid"])
        return admins

    def fetchUserIndicators(self,chatId):
        return list(self.userIndicators.find({"chatid": chatId}))

    def fetchUserStrategySignals(self,chatId):
        return list(self.signalStrategies.find({"chatid": chatId}))

    def removeRates(self):
        self.rates.remove({})
    
    def fetchMembers(self):
        members = []
        for member in self.members.find({}):
            members.append(member["chatid"])
        return members

    def isFreeVip(self):
        if self.rules.find_one({'rule':'freevip','expire_at':{'$gt':time.time()}}) != None:
            return True
        else:
            return False
    
    def fetchVipMembers(self):
        members = []
        for member in self.vip.find({'expire':{'$gte':time.time()}}):
            members.append(member["chatid"])
        return members
        
    def fetchStrategySignals(self,symbol):
        return list(self.strategySignals.find({"symbol": symbol}))
    
    def fetchPortfo(self,chatid):
        return list(self.portfo.find({"chatid": chatid}))
    
    def fetchWatchList(self, chatid):
        return list(self.watchList.find({"chatid": chatid}))

    def fetchSymbols(self):
        symbols = self.symbols.find_one()
        return symbols["symbols"]
    
    def updateRate(self,symbol,resolution,rates):
        self.rates.update_one({"symbol": symbol ,"rates.resolution" : resolution},{"$set": {"rates.$": rates}},upsert=True)
        
    def fetchRate(self,symbol):
        return self.rates.find_one({"symbol": symbol})

    def fetchRates(self):
        return self.rates.find({})

    def updateSymbols(self,symbols):
        data = {}
        lastUpdate = datetime.datetime.now()
        data["symbols"] = symbols
        data["date"] = lastUpdate
        self.symbols.update_one({},{"$set": data},upsert=True)    
    
    def updateRates(self,rates):
        lastUpdate = datetime.datetime.now()
        rates["date"] = lastUpdate
        self.rates.update_one({"symbol": rates["symbol"]},{"$set": rates},upsert=True)


    def insertStrategySignals(self,symbol,photo,message,onSupport,volumeIncreased,category, buy, resolution):
        message.replace("\n","\\n")
        today = datetime.datetime.today()
        timestamp = datetime.datetime.utcnow()
        data = {
            "resolution": resolution,
            "category" : category,
            "symbol" : symbol,
            "buy": buy,
            "photo" : photo,
            "message" : message,
            "onSupport" : onSupport,
            "volumeIncreased" : volumeIncreased,
            "date" : today,
            "expire" : timestamp,
        }
        self.strategySignals.insert_one(data)

    def removeStrategySignal(self,symbol):
        self.strategySignals.remove({"symbol": symbol})

    def updateIndicator(self, symbol, indicator, photo, message, onSupport, volumeIncreased, buy, resolution):
        message.replace("\n","\\n")
        today = datetime.datetime.today()
        timestamp = datetime.datetime.utcnow()
        data = {
            "resolution": resolution,
            "symbol" : symbol,
            "indicator" : indicator,
            "buy": buy,
            "photo" : photo,
            "message" : message,
            "onSupport" : onSupport,
            "volumeIncreased" : volumeIncreased,
            "date" : today,
            "expire" : timestamp,
        }
        self.indicators.update_one({"symbol": symbol, "indicator": indicator},{"$set": data},upsert=True)

    def fetchIndicators(self, symbol, resolution= None):
        if resolution == None:
            return list(self.indicators.find({"symbol": symbol}))
        else:
            return list(self.indicators.find({"symbol": symbol, "resolution": resolution}))
    
    def removeIndicator(self,symbol):
        self.indicators.remove({"symbol": symbol})
        
    def fetchBasketSignalsId(self):
        baskets = list()
        users = self.vip.find({"expire":{"$gte":time.time()}})
        for user in users:
            find_user = self.baskets.find_one({"chatid":user["chatid"]})
            if find_user is not None:
                baskets.append(find_user)
        return baskets

    def fetchPortfoSignalsId(self):
        portfo = list()
        users = self.vip.find({"expire":{"$gte":time.time()}})
        for user in users:
            find_user = self.userportfo.find({"chatid":user["chatid"]})
            if find_user is not None:
                for x in find_user:
                    portfo.append(x)
        return portfo

