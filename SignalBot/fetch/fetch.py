from db.db import Db
import datetime
import pandas as pd
import numpy as np
import requests
import re
from time import sleep
from numpy import array_split
from plot.plot import Plot
from telegram.signals import Signals
from telegram.telegram import Telegram
from numpy import array_split
from threading import Thread

class Fetch:
    def __init__(self):
        self.db = Db()

    def fetchSymbols(self):
        url = "https://api.binance.com/api/v3/exchangeInfo"
        symbols = requests.get(url).json()
        updateSymbols = []
        for symbol in symbols["symbols"]:
            if re.search(r"\w*USDT\b",symbol["symbol"]) != None and symbol["status"] == "TRADING":
                updateSymbols.append(symbol["symbol"])
        self.db.updateSymbols(updateSymbols)
    
    def symbolsUpdater(self):
        while True:
            self.fetchSymbols()
            sleep(3600*12)
        
    def updateResolution(self,symbol,resolution):
        db = self.db
        limit = 290
        url = f"https://api.binance.com/api/v3/klines?symbol={symbol}&interval={resolution}&limit={limit}"
        res = requests.get(url).json()
        time = []
        close = []
        open = []
        high = []
        low = []
        volume = []
        for rate in res:
            time.append(int(rate[6]))
            close.append(float(rate[4]))
            open.append(float(rate[1]))
            high.append(float(rate[2]))
            low.append(float(rate[3]))
            volume.append(float(rate[5]))
        resRate = {}
        resRate["resolution"] = resolution
        resRate["time"] = time
        resRate["close"] = close
        resRate["open"] = open
        resRate["high"] = high 
        resRate["low"] = low
        resRate["volume"] = volume
        db.updateRate(symbol,resolution,resRate)
        db.removeIndicator(symbol)
        db.removeStrategySignal(symbol)
        plot = Plot(symbol,time,close,open,high,low,volume,resolution)
        plot.indicators()
        signals = Signals(symbol)
        signals.sendIndictors()
        plot.strategy()
        signals.sendStrategySignals()
        signals.sendWatchListAndPortfo(symbol)
        

    def updateResolutions(self,symbols):
        resolutions = ["15m","1h","4h","1d"]
        limit = 290
        for symbol in symbols:
            rates = {}
            rates["symbol"] = symbol
            rates["rates"] = []
            for resolution in resolutions:
                url = f"https://api.binance.com/api/v3/klines?symbol={symbol}&interval={resolution}&limit={limit}"
                res = requests.get(url).json()
                time = []
                close = []
                open = []
                high = []
                low = []
                volume = []
                for rate in res:
                    time.append(int(rate[6]))
                    close.append(float(rate[4]))
                    open.append(float(rate[1]))
                    high.append(float(rate[2]))
                    low.append(float(rate[3]))
                    volume.append(float(rate[5]))
                resRate = {}
                resRate["resolution"] = resolution
                resRate["time"] = time
                resRate["close"] = close
                resRate["open"] = open
                resRate["high"] = high 
                resRate["low"] = low
                resRate["volume"] = volume
                rates["rates"].append(resRate)
            self.db.updateRates(rates)

    def updater(self, symbols):
        for symbol in symbols:
            for rate in symbol["rates"]:
                resolution = rate["resolution"]
                dateClose = datetime.datetime.fromtimestamp(float(rate["time"][-1]) / 1000.0, tz=datetime.timezone.utc) + datetime.timedelta(seconds = 10)
                now = datetime.datetime.now(tz=datetime.timezone.utc)
                if(dateClose < now):
                    self.updateResolution(symbol["symbol"], resolution)

    def resolutionsUpdater(self):
        try:
            while True:
                threads = []
                symbolsChunk = array_split(list(self.db.fetchRates()), 5)
                for symbols in symbolsChunk:
                    threads.append(Thread(target=self.updater,args=[symbols,]))
                for x in threads:
                    x.start()
                for i in threads:
                    i.join() 
        except:
            self.resolutionsUpdater()
           

    def fetchRates(self):
        self.fetchSymbols()
        db = self.db
        db.removeRates()
        symbols = self.db.fetchSymbols()
        self.updateResolutions(symbols)

    def main(self):
        # self.fetchRates()
        self.resolutionsUpdater()

