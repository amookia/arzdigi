from db.db import Db
import requests
import urllib
import config

class Telegram:
    def __init__(self):
        self.tToken = config.token
        self.db = Db()
        self.admins = self.db.fetchAdmins()

    def sendMessageToAdmin(self,message):
        text = message
        messageIds = []
        for admin in self.admins:
            tel_url = 'https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}'.format(self.tToken,str(admin["chatid"]),urllib.parse.quote(text))
            res = requests.get(tel_url).json()
            messageId = res["result"]["message_id"]
            messageIds.append(messageId)
        return messageIds
        
    def sendMessage(self,chatIds,message):
        text = message
        messageIds = []
        for id in chatIds:
            tel_url = 'https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}'.format(self.tToken,str(id),urllib.parse.quote(text))
            res = requests.get(tel_url).json()
            messageId = res["result"]["message_id"]
            messageIds.append(messageId)
        return messageIds
        
    def editMessage(self,chatIds,messageIds,message):
        text = message
        newMessageIds = []
        for idx in range(len(chatIds)):
            tel_url = 'https://api.telegram.org/bot{}/editMessageText?chat_id={}&message_id={}&text={}'.format(self.tToken,str(chatIds[idx]),int(messageIds[idx]),urllib.parse.quote(text))
            res = requests.get(tel_url).json()
            newMessageId = res["result"]["message_id"]
            newMessageIds.append(newMessageId)
        return newMessageIds

    def sendSignal(self,signal,chatId):
        text = signal['message'].replace('\\n','\n')
        tel_url = 'https://api.telegram.org/bot{}/sendPhoto?chat_id={}&caption={}'.format(self.tToken,str(chatId),urllib.parse.quote(text))
        files = {'photo':signal['photo']}
        requests.post(tel_url,files=files)

    def copyMessage(self, to_chatid,from_chatid,messageid,reply_to_messageid=None):
        url = f'https://api.telegram.org/bot{self.tToken}/' + 'copyMessage?from_chat_id=' + str(from_chatid) + '&message_id=' + str(messageid) + '&chat_id=' + str(to_chatid)
        if reply_to_messageid is not None : url += '&reply_to_message_id=' + str(reply_to_messageid)
        print(requests.get(url).text, url)

    def sendErrorMessageToAdmin(self, error, file):
        text = f"Error from line {error.__traceback__.tb_lineno}\nfile {file}\n{error}"
        messageIds = []
        for admin in self.admins:
            tel_url = 'https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}'.format(self.tToken,str(admin["chatid"]),urllib.parse.quote(text))
            res = requests.get(tel_url).json()
            messageId = res["result"]["message_id"]
            messageIds.append(messageId)
        return messageIds