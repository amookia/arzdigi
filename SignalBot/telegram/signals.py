from db.db import Db
from telegram.telegram import Telegram
import config

class Signals:
    def __init__(self,symbol):
        self.db = Db()
        self.symbol = symbol
        self.telegram = Telegram()
        self.admins = self.db.fetchAdmins()
        self.isFree = self.db.isFreeVip()
        self.vipMembers = self.db.fetchMembers() if self.isFree == True else self.db.fetchVipMembers()
        self.vipMembers = list(set(self.vipMembers+self.admins))
        self.tToken = config.token

    def sendIndictors(self):
        try:
            indicators = self.db.fetchIndicators(self.symbol)
            for user in self.vipMembers:
                settings = self.db.fetchUserIndicators(user)
                for setting in settings:
                    for key in setting.keys():
                        for indicator in indicators:
                            if key == indicator["indicator"] and indicator["resolution"] in setting["resolutions"]:
                                if setting[key] == True:
                                    if indicator["buy"] == True and setting["buy"] == True or indicator["buy"] == False and setting["sell"] == True:
                                        self.telegram.sendSignal(indicator, setting["chatid"])
        except Exception as e:
            self.telegram.sendErrorMessageToAdmin(e,__file__)
        
    def sendWatchListAndPortfo(self, symbol):
        try:
            for user in self.vipMembers:
                watchList = self.db.fetchWatchList(user)
                if len(watchList) != 0:
                    watchList = watchList[0]
                    for currency in watchList["currencies"]:
                        if currency == symbol:
                            indicators = self.db.fetchIndicators(currency)
                            strategies = self.db.fetchStrategySignals(currency)
                            for indicator in indicators:
                                for key in watchList.keys():
                                    if indicator["resolution"] == key and watchList[key] == True:
                                        self.telegram.sendSignal(indicator, watchList["chatid"])
                            for strategy in strategies:
                                for key in watchList.keys():
                                    if  strategy["resolution"] == key and watchList[key] == True:
                                        self.telegram.sendSignal(strategy, watchList["chatid"])
                    portfo = self.db.fetchPortfo(user)
                    for x in portfo:
                        if symbol == x["symbol"] and x["exit"] == False:
                            indicators = self.db.fetchIndicators(x["symbol"])
                            strategies = self.db.fetchStrategySignals(x["symbol"])
                            for indicator in indicators:
                                self.telegram.sendSignal(indicator, x["chatid"])
                            for strategy in strategies:
                                self.telegram.sendSignal(strategy, x["chatid"])
                    
        except Exception as e:
            self.telegram.sendErrorMessageToAdmin(e,__file__)

    def sendStrategySignals(self):
        try:
            strategies = self.db.fetchStrategySignals(self.symbol)
            for user in self.vipMembers:
                settings = self.db.fetchUserStrategySignals(user)
                for setting in settings:
                    for key in setting.keys():
                        for strategy in strategies:
                            if key == strategy["category"] and strategy["resolution"] in setting["resolutions"]:
                                if strategy["buy"] == True and setting["buy"] == True or strategy["buy"] == False and setting["sell"] == True:
                                    self.telegram.sendSignal(strategy, setting["chatid"])
        except Exception as e:
            self.telegram.sendErrorMessageToAdmin(e, __file__)
