# Run it on Docker container
```bash
docker rm -f signals && docker rmi -f signal-image
docker build -t signal-image .
docker run --name signals --network tahlilbot --restart always -d signal-image
```
