from plot.indicators.ema import Ema
from plot.indicators.rsi import Rsi
from plot.indicators.cci import Cci
from plot.indicators.stochastic import Stochastich
from plot.indicators.macd import Macd
from plot.indicators.psar import Psar
from plot.indicators.bollinger import Bollinger
from plot.indicators.ichimoku import Ichimoku
from plot.indicators.ohcl import OHCL
from db.db import Db
import datetime
import numpy as np

class Plot:
    def __init__(self, symbol, time, close, open, high, low, volume, resolution):
        self.symbol = symbol
        self.time =  np.array(list(map(lambda item: datetime.datetime.fromtimestamp(item/1000), time[:-1])))
        self.close = close[:-1]
        self.high = high[:-1]
        self.low = low[:-1]
        self.volume = volume[:-1]
        self.open = open[:-1]
        self.resolution = resolution
        self.db = Db()
    
    def strategy(self):
        signals = self.db.fetchIndicators(self.symbol, self.resolution)
        indicators = {"indicators": [],"buy": []}

        for x in signals:
            indicators["indicators"].append(x["indicator"])
            indicators["buy"].append(x["buy"])
        
        if len(set(indicators["buy"])) == 1:
            buy = indicators["buy"][0]
            
            if 'stochastic' in indicators["indicators"] and 'bollinger' in indicators["indicators"]:
                strategy = OHCL(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution)
                strategy.load('bs', buy)

            elif 'rsi' in indicators["indicators"] and 'bollinger' in indicators["indicators"]:
                strategy = OHCL(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution)
                strategy.load('br', buy)
                
            elif 'rsi' in indicators["indicators"] and 'stochastic' in indicators["indicators"] and 'macd' in indicators["indicators"]:
                strategy = OHCL(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution)
                strategy.load('rsm', buy)
            
            elif 'ema' in indicators["indicators"] and 'cci' in indicators["indicators"]:
                strategy = OHCL(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution)
                strategy.load('ecc', buy)
        
    
    def indicators(self):
        Ema(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution).load()
        Rsi(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution).load()
        Cci(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution).load()
        Stochastich(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution).load()
        Macd(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution).load()
        Psar(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution).load()
        Bollinger(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution).load()
        Ichimoku(self.symbol, self.time, self.close, self.open, self.high, self.low, self.volume, self.resolution).load()
