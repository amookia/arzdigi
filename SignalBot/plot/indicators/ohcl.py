import plotly.graph_objects as go
from plot.layout.layout import Layout
from plotly.subplots import make_subplots

class OHCL(Layout):
    def load(self, category, buy):
        fig = make_subplots(specs=[[{"secondary_y": True}]])
        fig.update_layout(title_text=self.symbol, title_x=0.5)
        fig.update(layout_xaxis_rangeslider_visible=False)
        fig.add_trace(self.candleStick, secondary_y=True)
        for idx,el in enumerate(self.pivots):
            if el > 0:
                if idx == 3:
                    fig.add_hline(y=el,line_color= "white", line_dash="dot", line_width=1, secondary_y=True)
                if idx > 3 :
                    fig.add_hline(y=el,line_color= "red", line_dash="dot", line_width=1, secondary_y=True)
                if idx < 3:
                    fig.add_hline(y=el,line_color= "green", line_dash="dot", line_width=1, secondary_y=True)
        fig.add_trace(self.volumeBars,secondary_y=False)
        fig.layout.yaxis2.showgrid=False
        message = self.message(buy, strategy= category)
        img_bytes = fig.to_image(format="png", width=1980, height=1080)
        self.db.insertStrategySignals(self.symbol,img_bytes,message,self.onSupport,self.volumeIncreased, category, buy, self.resolution)