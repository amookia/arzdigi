from plot.layout.layout import Layout
from db.db import Db
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pandas_ta import ichimoku, cross
import numpy as np
import pandas as pd

class Ichimoku(Layout):
    def load(self):
        high = pd.Series(np.array(self.high,dtype=float))
        low = pd.Series(np.array(self.low,dtype=float))
        close = pd.Series(np.array(self.close,dtype=float))
        ICHIMOKU = ichimoku(high,low,close)
        ISA_9 = ICHIMOKU[0]['ISA_9']
        ISB_26 = ICHIMOKU[0]['ISB_26']
        ITS_9 = ICHIMOKU[0]['ITS_9']
        IKS_26 = ICHIMOKU[0]['IKS_26']
        ICS_26 = ICHIMOKU[0]['ICS_26']
        buy = cross(ITS_9,IKS_26)
        sell = cross(ITS_9,IKS_26,False)
        up = np.where(buy == True)
        down = np.where(sell == True)
        date_up = [self.date[i] for i in up][0].tolist()
        date_down = [self.date[i] for i in down][0].tolist()
        up = [ITS_9[i] for i in up][0].tolist()
        down = [ITS_9[i] for i in down][0].tolist()
        if self.date[-1] in date_up or self.date[-1] in date_down:
            fig = make_subplots(specs=[[{"secondary_y": True}]])
            fig.update_layout(title_text=self.symbol, title_x=0.5)
            fig.update(layout_xaxis_rangeslider_visible=False)
            fig.add_trace(self.candleStick
            ,secondary_y=True)
            for idx,el in enumerate(self.pivots):
                if el > 0:
                    if idx == 3:
                        fig.add_hline(y=el,line_color= "white", line_dash="dot", line_width=1, secondary_y=True)
                    if idx > 3 :
                        fig.add_hline(y=el,line_color= "red", line_dash="dot", line_width=1, secondary_y=True)
                    if idx < 3:
                        fig.add_hline(y=el,line_color= "green", line_dash="dot", line_width=1, secondary_y=True)
            span_a = ISA_9.tolist()
            span_b = ISB_26.tolist()
            self.df = pd.DataFrame()

            self.df["span_a"] = span_a
            self.df["span_b"] = span_b
            self.df['span_c'] = np.nan
            self.data = []

            for index, row in self.df.iterrows():
                if row['span_a'] < row['span_b']:
                    self.df.at[index,'span_c'] = row['span_a']
                elif row['span_a'] > row['span_b']:
                    self.df.at[index,'span_c'] = row['span_b']
                elif row['span_a'] == row['span_b']:
                    self.df.at[index,'span_c'] = row['span_b']

            if self.df.__contains__('span_c'):
                trace = go.Scatter(
                x = self.date,
                y = self.df['span_c'],
                showlegend=False,
                line = dict(color = ('rgba(255, 255, 255, 0)')))
                self.data.append(trace)

            if self.df.__contains__('span_b'):
                trace = go.Scatter(
                    x = self.date,
                    y = self.df['span_b'],
                    fill = 'tonexty',
                    name = "SPAN B",
                    fillcolor = 'rgba(255, 0, 0, 0.3)',
                    line = dict(color = ('rgba(255, 0, 0, 0.3)')))
                self.data.append(trace)

            if self.df.__contains__('span_c'):
                trace = go.Scatter(
                    x = self.date,
                    y = self.df['span_c'],
                    showlegend=False,
                    line = dict(color = ('rgba(255, 255, 255, 0)')))
                self.data.append(trace)

            if self.df.__contains__('span_a'):
                trace = go.Scatter(
                    x = self.date,
                    y = self.df['span_a'],
                    fill = 'tonexty',
                    name = "SPAN A",
                    fillcolor = 'rgba(0, 255, 0, 0.3)',
                    line = dict(color = ('rgba(0, 255, 0, 0.3)')))
                self.data.append(trace)

            for tr in self.data:
                fig.add_trace(tr,secondary_y=True)    

            fig.add_trace(go.Scatter(name= "TENKAN", x=self.date, y=ITS_9, line=dict(color='blue', width=1)),secondary_y=True)
            fig.add_trace(go.Scatter(name= "KIJUN", x=self.date, y=IKS_26, line=dict(color='red', width=1)),secondary_y=True)
            fig.add_trace(go.Scatter(name= "SENKOU", x=self.date, y=ICS_26, line=dict(color='yellow', width=1)),secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_up,
                y=up,
                marker= dict(
                    color='green',
                    size=20,
                    symbol = 'arrow-up',
                ),
                showlegend=False
            ),secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_down,
                y=down,
                marker=dict(
                    color="red",
                    size=20,
                    symbol = 'arrow-down',
                ),
                showlegend=False
            ),secondary_y=True)
            fig.add_trace(self.volumeBars,secondary_y=False)
            fig.layout.yaxis2.showgrid=False
            if self.date[-1] in date_up:
                buy = True
            elif self.date[-1] in date_down:
                buy =  False
            message = self.message(buy,"ICHIMOKU ")
            img_bytes = fig.to_image(format="png", width=1980, height=1080)
            self.db.updateIndicator(self.symbol,'ichimoku',img_bytes,message,self.onSupport,self.volumeIncreased,buy,self.resolution)
            
