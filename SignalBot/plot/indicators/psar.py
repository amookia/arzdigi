from plot.layout.layout import Layout
from db.db import Db
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pandas_ta import psar
import numpy as np
import pandas as pd

class Psar(Layout):
    def load(self):
        high = pd.Series(np.array(self.high,dtype=float))
        low = pd.Series(np.array(self.low,dtype=float))
        close = pd.Series(np.array(self.close,dtype=float))
        PSAR = psar(high,low,close)
        psarbear = PSAR["PSARs_0.02_0.2"]
        psarbull = PSAR["PSARl_0.02_0.2"]
        buyPsar, sellPsar = self.psar_signals(psarbull,psarbear)
        buyPsar = np.where(buyPsar == True)
        sellPsar = np.where(sellPsar == True)
        date_up = [self.date[i] for i in buyPsar][0].tolist()
        date_down = [self.date[i] for i in sellPsar][0].tolist()
        buyPsar = [psarbull[i] for i in buyPsar][0].tolist()
        sellPsar = [psarbear[i] for i in sellPsar][0].tolist()
        psarbull = psarbull.fillna(value=np.nan)
        psarbear = psarbear.fillna(value=np.nan)
        if self.date[-1] in date_up or self.date[-1] in date_down:
            fig = make_subplots(specs=[[{"secondary_y": True}]])
            fig.update_layout(title_text=self.symbol, title_x=0.5)
            fig.update(layout_xaxis_rangeslider_visible=False)
            fig.add_trace(self.candleStick, secondary_y=True)
            for idx,el in enumerate(self.pivots):
                if el > 0:
                    if idx == 3:
                        fig.add_hline(y=el,line_color= "white", line_dash="dot", line_width=1, secondary_y=True)
                    if idx > 3 :
                        fig.add_hline(y=el,line_color= "red", line_dash="dot", line_width=1, secondary_y=True)
                    if idx < 3:
                        fig.add_hline(y=el,line_color= "green", line_dash="dot", line_width=1, secondary_y=True)
            fig.add_trace(go.Scatter(name= "BEAR", x=self.date, y=psarbull, line=dict(color='red', width=4, dash='dot')),secondary_y=True)
            fig.add_trace(go.Scatter(name= "BULL", x=self.date, y=psarbear, line=dict(color='blue', width=4, dash='dot')),secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_up,
                y=buyPsar,
                marker= dict(
                    color='green',
                    size=20,
                    symbol = 'arrow-up',
                ),
                showlegend=False
            ),secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_down,
                y=sellPsar,
                marker=dict(
                    color="red",
                    size=20,
                    symbol = 'arrow-down',
                ),
                showlegend=False
            ),secondary_y=True)
            fig.add_trace(self.volumeBars,secondary_y=False)
            fig.layout.yaxis2.showgrid= False
            if self.date[-1] in date_up:
                buy = True
            elif self.date[-1] in date_down:
                buy =  False
            message = self.message(buy,"PSAR")
            img_bytes = fig.to_image(format="png", width=1980, height=1080)
            self.db.updateIndicator(self.symbol,'psar',img_bytes,message,self.onSupport,self.volumeIncreased,buy,self.resolution)
                
    def psar_signals(self,bull,bear):
        buyPsar = bull.notna() & bull.shift(1).isna()
        sellPsar = bear.notna() & bear.shift(1).isna()
        return buyPsar, sellPsar

