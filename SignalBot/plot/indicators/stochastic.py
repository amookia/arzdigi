from plot.layout.layout import Layout
from db.db import Db
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pandas_ta import stoch, cross, cross_value
import numpy as np
import pandas as pd
from plotly.subplots import make_subplots

class Stochastich(Layout):
    def load(self):
        high = pd.Series(np.array(self.high,dtype=float))
        low = pd.Series(np.array(self.low,dtype=float))
        close = pd.Series(np.array(self.close,dtype=float))        
        STOCH = stoch(high,low,close,smooth_k=1)
        STOCHK = STOCH['STOCHk_14_3_1']
        STOCHD = STOCH['STOCHd_14_3_1']
        buy = cross(STOCHK,STOCHD)
        sell = cross(STOCHK,STOCHD,False)
        up = cross_value(STOCHK,30)
        down = cross_value(STOCHK,70,False)

        buy = np.where(buy == True)[0].tolist()
        sell = np.where(sell == True)[0].tolist()
        up = np.where(up == True)[0].tolist()
        down = np.where(down == True)[0].tolist()
        buy = list(set(buy) & set(up))
        sell = list(set(sell) & set(down))
        date_up = [self.date[i] for i in buy]
        date_down = [self.date[i] for i in sell]
        up = [STOCHK[i] for i in buy]
        down = [STOCHD[i] for i in sell]
        if self.date[-1] in date_up or self.date[-1] in date_down:
            fig = make_subplots(rows=2, cols=1, shared_xaxes=True, 
                vertical_spacing=0.05, subplot_titles=('', ''), 
                row_width=[0.2, 0.7], specs=[[{"secondary_y": True}],[{"secondary_y": True}]])
            fig.update_layout(title_text=self.symbol, title_x=0.5)
            fig.update(layout_xaxis_rangeslider_visible=False)
            fig.add_trace(self.candleStick, row=1, col=1, secondary_y=True)
            fig.add_trace(go.Scatter(name= "STOCHK", x=self.date, y=STOCHK, line_color="blue"), row=2, col=1)
            fig.add_trace(go.Scatter(name= "STOCHD", x=self.date, y=STOCHD, line_color="red"), row=2, col=1)
            for idx,el in enumerate(self.pivots):
                if el > 0:
                    if idx == 3:
                        fig.add_hline(y=el,line_color= "white", line_dash="dot", line_width=1, secondary_y=True)
                    if idx > 3 :
                        fig.add_hline(y=el,line_color= "red", line_dash="dot", line_width=1, secondary_y=True)
                    if idx < 3:
                        fig.add_hline(y=el,line_color= "green", line_dash="dot", line_width=1, secondary_y=True)
            fig.add_trace(self.volumeBars,secondary_y=False)
            fig.add_hrect(y0=30, y1=70,  line_width=0, fillcolor="blue", opacity=0.2, row=2, col=1)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_up,
                y=up,
                marker= dict(
                    color='green',
                    size=20,
                    symbol = 'arrow-up',
                ),
                showlegend=False
            ), row=2, col=1, secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_down,
                y=down,
                marker=dict(
                    color="red",
                    size=20,
                    symbol = 'arrow-down',
                ),
                showlegend=False
            ), row=2, col=1, secondary_y=True)
            fig.layout.yaxis2.showgrid=False
            if self.date[-1] in date_up:
                buy = True
            elif self.date[-1] in date_down:
                buy =  False
            message = self.message(buy,"STOCHASTICH")
            img_bytes = fig.to_image(format="png", width=1980, height=1080)
            self.db.updateIndicator(self.symbol,'stochastic',img_bytes,message,self.onSupport,self.volumeIncreased,buy,self.resolution)
