from plot.layout.layout import Layout
from db.db import Db
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pandas_ta import macd, cross
import numpy as np
import pandas as pd

class Macd(Layout):
    def load(self):
        close = pd.Series(np.array(self.close,dtype=float))        
        MACD = macd(close)
        MACD_12_26_9 = MACD['MACD_12_26_9']
        MACDh_12_26_9 = MACD['MACDh_12_26_9']
        MACDs_12_26_9 = MACD['MACDs_12_26_9']
        buy = cross(MACD_12_26_9,MACDs_12_26_9)
        sell = cross(MACD_12_26_9,MACDs_12_26_9,False)
        up = np.where(buy == True)
        down = np.where(sell == True)
        date_up = [self.date[i] for i in up][0].tolist()
        date_down = [self.date[i] for i in down][0].tolist()
        up = [MACD_12_26_9[i] for i in up] [0].tolist()
        down = [MACD_12_26_9[i] for i in down][0].tolist()
        if self.date[-1] in date_up or self.date[-1] in date_down:
            fig = make_subplots(rows=2, cols=1, shared_xaxes=True, 
                vertical_spacing=0.05, subplot_titles=('', ''), 
                row_width=[0.2, 0.7], specs=[[{"secondary_y": True}],[{"secondary_y": True}]])
            fig.update_layout(title_text=self.symbol, title_x=0.5)
            fig.update(layout_xaxis_rangeslider_visible=False)
            fig.add_trace(self.candleStick, row=1, col=1, secondary_y=True)
            fig.add_trace(go.Scatter(name= "MACD", x=self.date, y=MACD_12_26_9, line_color="orange"), row=2, col=1)
            fig.add_trace(go.Scatter(name= "SIGNAL", x=self.date, y=MACDs_12_26_9, line_color="yellow"), row=2, col=1)
            fig.add_trace(go.Bar(name= "Bar", x=self.date, y=MACDh_12_26_9, opacity= 0.2), secondary_y=False, row=2, col=1)
            for idx,el in enumerate(self.pivots):
                if el > 0:
                    if idx == 3:
                        fig.add_hline(y=el,line_color= "white", line_dash="dot", line_width=1, secondary_y=True)
                    if idx > 3 :
                        fig.add_hline(y=el,line_color= "red", line_dash="dot", line_width=1, secondary_y=True)
                    if idx < 3:
                        fig.add_hline(y=el,line_color= "green", line_dash="dot", line_width=1, secondary_y=True)
            fig.add_trace(self.volumeBars,secondary_y=False)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_up,
                y=up,
                marker= dict(
                    color='green',
                    size=20,
                    symbol = 'arrow-up',
                ),
                showlegend=False
            ), row=2, col=1, secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_down,
                y=down,
                marker=dict(
                    color="red",
                    size=20,
                    symbol = 'arrow-down',
                ),
                showlegend=False
            ), row=2, col=1, secondary_y=True)
            fig.layout.yaxis2.showgrid=False
            if self.date[-1] in date_up:
                buy = True
            elif self.date[-1] in date_down:
                buy =  False
            message = self.message(buy,"MACD")
            img_bytes = fig.to_image(format="png", width=1980, height=1080)
            self.db.updateIndicator(self.symbol,'macd',img_bytes,message,self.onSupport,self.volumeIncreased,buy,self.resolution)
