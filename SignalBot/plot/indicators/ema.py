from plot.layout.layout import Layout
from db.db import Db
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pandas_ta import ema, cross
import numpy as np
import pandas as pd

class Ema(Layout):
    def load(self):
        close = pd.Series(np.array(self.close,dtype=float))
        EmaS = ema(close,length=9)
        EmaL = ema(close,length=26)
        buy = cross(EmaS,EmaL)
        sell = cross(EmaS,EmaL,False)
        up = np.where(buy == True)
        down = np.where(sell == True)
        date_up = [self.date[i] for i in up][0].tolist()
        date_down = [self.date[i] for i in down][0].tolist()
        up = [EmaS[i] for i in up][0].tolist()
        down = [EmaS[i] for i in down][0].tolist()
        if self.date[-1] in date_up or self.date[-1] in date_down:
            fig = make_subplots(specs=[[{"secondary_y": True}]])
            fig.update_layout(title_text=self.symbol, title_x=0.5)
            fig.update(layout_xaxis_rangeslider_visible=False)
            fig.add_trace(self.candleStick
            ,secondary_y=True)
            for idx,el in enumerate(self.pivots):
                if el > 0:
                    if idx == 3:
                        fig.add_hline(y=el,line_color= "white", line_dash="dot", line_width=1, secondary_y=True)
                    if idx > 3 :
                        fig.add_hline(y=el,line_color= "red", line_dash="dot", line_width=1, secondary_y=True)
                    if idx < 3:
                        fig.add_hline(y=el,line_color= "green", line_dash="dot", line_width=1, secondary_y=True)
            fig.add_trace(go.Scatter(name= "EMA9", x=self.date, y=EmaS, line=dict(color='orange', width=1)),secondary_y=True)
            fig.add_trace(go.Scatter(name= "EMA26", x=self.date, y=EmaL, line=dict(color='red', width=1)),secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_up,
                y=up,
                marker= dict(
                    color='green',
                    size=20,
                    symbol = 'arrow-up',
                ),
                showlegend=False
            ),secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_down,
                y=down,
                marker=dict(
                    color="red",
                    size=20,
                    symbol = 'arrow-down',
                ),
                showlegend=False
            ),secondary_y=True)
            fig.add_trace(self.volumeBars,secondary_y=False)
            fig.layout.yaxis2.showgrid=False
            if self.date[-1] in date_up:
                buy = True
            elif self.date[-1] in date_down:
                buy =  False
            message = self.message(buy,"EMA")
            img_bytes = fig.to_image(format="png", width=1980, height=1080)
            self.db.updateIndicator(self.symbol,'ema',img_bytes,message,self.onSupport,self.volumeIncreased,buy,self.resolution)
