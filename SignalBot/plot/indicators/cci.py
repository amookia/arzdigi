from plot.layout.layout import Layout
from db.db import Db
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pandas_ta import cci, cross_value
import numpy as np
import pandas as pd

class Cci(Layout):
    def load(self):
        high = pd.Series(np.array(self.high,dtype=float))
        low = pd.Series(np.array(self.low,dtype=float))
        close = pd.Series(np.array(self.close,dtype=float))
        CCI = cci(high,low,close)
        buy = cross_value(CCI,-100)
        sell = cross_value(CCI,100,False)
        buy = np.where(buy == True)
        sell = np.where(sell == True)
        date_up = [self.date[i] for i in buy][0].tolist()
        date_down = [self.date[i] for i in sell][0].tolist()
        up = [CCI[i] for i in buy][0].tolist()
        down = [CCI[i] for i in sell][0].tolist()
        if self.date[-1] in date_up or self.date[-1] in date_down:
            fig = make_subplots(rows=2, cols=1, shared_xaxes=True, 
                vertical_spacing=0.05, subplot_titles=('', ''), 
                row_width=[0.2, 0.7], specs=[[{"secondary_y": True}],[{"secondary_y": True}]])
            fig.update_layout(title_text=self.symbol, title_x=0.5)
            fig.update(layout_xaxis_rangeslider_visible=False)
            fig.add_trace(self.candleStick , row=1, col=1, secondary_y=True)
            fig.add_trace(go.Scatter(name= "CCI", x=self.date, y=CCI, line_color="yellow"), row=2, col=1)
            for idx,el in enumerate(self.pivots):
                if el > 0:
                    if idx == 3:
                        fig.add_hline(y=el,line_color= "white", line_dash="dot", line_width=1, secondary_y=True)
                    if idx > 3 :
                        fig.add_hline(y=el,line_color= "red", line_dash="dot", line_width=1, secondary_y=True)
                    if idx < 3:
                        fig.add_hline(y=el,line_color= "green", line_dash="dot", line_width=1, secondary_y=True)
            fig.add_trace(self.volumeBars,secondary_y=False)
            fig.add_hrect(y0=-100, y1=100,  line_width=0, fillcolor="yellow", opacity=0.2, row=2, col=1)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_up,
                y=up,
                marker= dict(
                    color='green',
                    size=20,
                    symbol = 'arrow-up',
                ),
                showlegend=False
            ), row=2, col=1, secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_down,
                y=down,
                marker=dict(
                    color="red",
                    size=20,
                    symbol = 'arrow-down',
                ),
                showlegend=False
            ), row=2, col=1, secondary_y=True)
            fig.layout.yaxis2.showgrid=False
            if self.date[-1] in date_up:
                buy = True
            elif self.date[-1] in date_down:
                buy =  False
            message = self.message(buy,"CCI")
            img_bytes = fig.to_image(format="png", width=1980, height=1080)
            self.db.updateIndicator(self.symbol,'cci',img_bytes,message,self.onSupport,self.volumeIncreased,buy,self.resolution)

    def cci_signals(self,CCI):
        sell = (CCI>100) & (CCI.shift(-1)<100)
        buy = (CCI<-100) & (CCI.shift(-1)>-100)
        return sell,buy