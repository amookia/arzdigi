from plot.layout.layout import Layout
from db.db import Db
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pandas_ta import bbands 
import numpy as np
import pandas as pd

class Bollinger(Layout):
    def load(self):
        close = pd.Series(np.array(self.close,dtype=float))
        BB = bbands(close,20,2)
        BBL = BB['BBL_20_2.0']
        BBM = BB['BBM_20_2.0']
        BBU = BB['BBU_20_2.0']
        buy, sell = self.bollinger_signals(BBU,BBL)
        buy = np.where(buy == True)
        sell = np.where(sell == True)
        date_up = [self.date[i] for i in buy][0].tolist()
        date_down = [self.date[i] for i in sell][0].tolist()
        up = [BBL[i] for i in buy][0].tolist() 
        down = [BBU[i] for i in sell][0].tolist()
        if self.date[-1] in date_up or self.date[-1] in date_down:
            fig = make_subplots(specs=[[{"secondary_y": True}]])
            fig.update_layout(title_text=self.symbol, title_x=0.5)
            fig.update(layout_xaxis_rangeslider_visible=False)
            fig.add_trace(self.candleStick
            ,secondary_y=True)
            for idx,el in enumerate(self.pivots):
                if el > 0:
                    if idx == 3:
                        fig.add_hline(y=el,line_color= "white", line_dash="dot", line_width=1, secondary_y=True)
                    if idx > 3 :
                        fig.add_hline(y=el,line_color= "red", line_dash="dot", line_width=1, secondary_y=True)
                    if idx < 3:
                        fig.add_hline(y=el,line_color= "green", line_dash="dot", line_width=1, secondary_y=True)
            fig.add_trace(go.Scatter(name= "BBL", x=self.date, y=BBL, line=dict(color='red', width=0)),secondary_y=True)
            fig.add_trace(go.Scatter(name= "BBM", x=self.date, y=BBM, line=dict(color='blue', width=0), fill='tonexty'),secondary_y=True)
            fig.add_trace(go.Scatter(name= "BBU", x=self.date, y=BBU, line=dict(color='red', width=0), fill='tonexty'),secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_up,
                y=up,
                marker= dict(
                    color='green',
                    size=20,
                    symbol = 'arrow-up',
                ),
                showlegend=False
            ),secondary_y=True)
            fig.add_trace(go.Scatter(
                mode='markers',
                x=date_down,
                y=down,
                marker=dict(
                    color="red",
                    size=20,
                    symbol = 'arrow-down',
                ),
                showlegend=False
            ),secondary_y=True)
            fig.add_trace(self.volumeBars,secondary_y=False),
            fig.layout.yaxis2.showgrid=False
            if self.date[-1] in date_up:
                buy = True
            elif self.date[-1] in date_down:
                buy =  False
            message = self.message(buy,"BB")
            img_bytes = fig.to_image(format="png", width=1980, height=1080)
            self.db.updateIndicator(self.symbol,'bollinger',img_bytes,message,self.onSupport,self.volumeIncreased,buy,self.resolution)
            
    def bollinger_signals(self,BU,BL):
        buy = (self.low < BL) & (self.close > self.open) & (self.close > BL)
        sell = (self.high > BU) & (self.close < self.open) & (self.close < BU)
        return buy, sell