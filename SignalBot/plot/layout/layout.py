import jdatetime
import datetime
# from messeage.message import Message
# from indicators.pivot import Pivot
from db.db import Db
import plotly.graph_objects as go
from pandas_ta import ema
import plotly.io as pio
import config
import numpy as np
import pandas as pd

pio.templates.default = "plotly_dark"

class Layout:
    def __init__(self,symbol,date,close,open,high,low,volume,resolution,text = ''):
        self.db = Db()
        self.symbol = symbol
        self.botName = config.botName
        self.botPName = config.botPName
        self.C = float(close[-2])
        self.H = float(max(high[:90]))
        self.L = float(min(low[:90]))
        self.PP = (self.H + self.L + self.C) / 3
        self.S1 = self.PP - ((self.H - self.L) * 0.382)
        self.S2 = self.PP - ((self.H - self.L) * 0.618)
        self.S3 =  self.PP - ((self.H - self.L) * 1)
        self.R1 =  self.PP + ((self.H - self.L) * 0.382)
        self.R2 = self.PP + ((self.H - self.L) * 0.618)
        self.R3 = self.PP + ((self.H - self.L) * 1.618)
        self.pivots = [self.S1,self.S2,self.S3,self.PP,self.R1,self.R2,self.R3]
        self.date = date
        self.close = close
        self.high = high
        self.low = low
        self.volume = volume
        self.open = open
        self.resolution = resolution
        self.today = jdatetime.date.today().strftime("%Y/%m/%d")
        self.candleStick = go.Candlestick(name="CANDLESTICK", x=self.date, open=self.open, high=self.high, low=self.low, close=self.close)
        self.volumeBars = go.Bar(name= "VOLUME", x=self.date, y=self.volume, opacity= 0.2)
        self.text = text
        self.db = Db()
        self.volumeIncreased = self.increasedVolume()
        self.onSupport = self.isOnSupport()
    
    def message(self,buy,indicator = '', strategy = None):
        self.pivot_message = self.pivotMessage(buy)
        message = ""
        if strategy == None:
            if buy == True:
                message = "📲 ارز : {}\n🕰 تایم فریم : {} \n🟢 خرید : {} \n📊 اندیکاتور : {} \n{}\n🗓 تاریخ : {} \n{}".format("#" + self.symbol, self.resolution, self.close[-1], indicator, self.pivot_message, self.today, self.botName)
            else:
                message = "📲 ارز : {}\n🕰 تایم فریم : {} \n🔴 فروش : {} \n📊 اندیکاتور : {} \n{}\n🗓 تاریخ : {} \n{}".format("#" + self.symbol, self.resolution, self.close[-1], indicator, self.pivot_message, self.today, self.botName)
        else:
            if buy == True:
                message = "🏆 سیگنال استراتژی \n📲 ارز : {}\n🕰 تایم فریم : {} \n🟢 خرید : {} \n📊 استراتژی : {} \n{}\n🗓 تاریخ : {} \n{}".format("#" + self.symbol, self.resolution, self.close[-1], strategy, self.pivot_message, self.today, self.botName)
            else:
                message = "🏆 سیگنال استراتژی \n📲 ارز : {}\n🕰 تایم فریم : {} \n🔴 فروش : {} \n📊 استراتژی : {} \n{}\n🗓 تاریخ : {} \n{}".format("#" + self.symbol, self.resolution, self.close[-1], strategy, self.pivot_message, self.today, self.botName)
            
        return message

    def isOnSupport(self):
        percent = 1
        s1 = self.S1 + (self.S1 / 100) * percent
        s2 = self.S2 + (self.S2 / 100) * percent
        s3 = self.S3 + (self.S3 / 100) * percent
        if self.close[-1] >= self.S1 and self.close[-1] < s1 or self.close[-1] >= self.S2 and self.close[-1] < s2 or self.close[-1] >= self.S3 and self.close[-1] < s3:
            return True
        else:
            return False

    def increasedVolume(self):
        if self.volume[0] > self.volume[1]:
            return True
        elif self.volume[0] < self.volume[1]:
            return False
    
    def pivotMessage(self, buy):

        volumeState = ''
        e = ''
        if self.volumeIncreased == True:
            e = '🔻'
        elif self.volumeIncreased == False:
            e = '🔺'
        volumeState = '🧮 حجم : ' + str(self.volume[0]) + e
        
        onSupport = ''

        if self.onSupport:
            onSupport = '✅'
        elif self.onSupport == False:
            onSupport = '❌'

        rlevels = [0, 0]

        if float(self.close[-1]) > self.R2 and float(self.close[-1]) < self.R3 :
            rlevels = [self.R2, self.R3]
        elif float(self.close[-1]) > self.R1 and float(self.close[-1]) < self.R2 :
            rlevels = [self.R1, self.R2]
        elif float(self.close[-1]) > self.PP and float(self.close[-1]) < self.R1 :
            rlevels = [self.PP, self.R1]
        elif float(self.close[-1]) > self.S1 and float(self.close[-1]) < self.PP :
            rlevels = [self.S1, self.PP]
        elif float(self.close[-1]) > self.S2 and float(self.close[-1]) < self.S1 :
            rlevels = [self.S2, self.S1]
        elif float(self.close[-1]) > self.S3 and float(self.close[-1]) < self.S2 :
            rlevels = [self.S3, self.S2]

        if buy:
            stopLoss =  str(round(rlevels[0], 8)) + " " +  str(abs(round((float(self.close[-1]) - rlevels[0]) / float(self.close[-1]) *  100,2))) + "%"
            takeProfit = str(round(rlevels[1], 8)) + " " +  str(abs(round((float(self.close[-1]) - rlevels[1]) /  float(self.close[-1]) *  100,2))) + "%"
        else:
            stopLoss =  str(round(rlevels[1], 8)) + " " +  str(abs(round((float(self.close[-1]) - rlevels[1]) / float(self.close[-1]) *  100,2))) + "%"
            takeProfit = str(round(rlevels[0], 8)) + " " +  str(abs(round((float(self.close[-1]) - rlevels[0]) /  float(self.close[-1]) *  100,2))) + "%"

        pivot_msg =  self.text + volumeState + '\n'+ '🔸 مقاومت ها : ' + str(round(self.R1, 8)) + " " + str(round(self.R2, 8)) + " " + str(round(self.R3, 8)) + '\n' + '🔹 حمایت ها : ' + str(round(self.S1, 8)) + " " + str(round(self.S2, 8)) + " " + str(round(self.S3, 8)) + '\n' + f'📌 حد ضرر : {stopLoss}' + '\n' + f'📌 حد سود : {takeProfit}' + '\n' + f'📏 نزدیک به حمایت : {onSupport}'
        
        return pivot_msg
    